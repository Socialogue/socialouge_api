import BrandModel from '../../models/brands.js';

export const getBrands = async (req, res) => {
    const { page, perPage } = req.query;
    const options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(perPage, 10) || 10,
    }
    try {
        const brands = await BrandModel.paginate( {}, options);
        res.status(200).json({success:true, data:brands, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const createBrand = async (req, res) => {
    const brand = req.body;
    const newBrand = new BrandModel(brand);
    try {
        await newBrand.save();
        res.status(201).json({success:true, data:newBrand, message:'Brand Successfully Created'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const getBrandById = async (req, res) => {
    try {
        const brand = await BrandModel.findById(req.params.brandId);
        res.status(200).json({success:true, data:brand, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const updateBrand = async (req, res) => {
    const brand = req.body;
    try {
        const updateBrand = await BrandModel.updateOne(
            { _id: req.params.brandId },
            { $set: brand } 
        );
        res.status(200).json({success:true, data:updateBrand, message:'Brands successfully updated'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const deleteBrand = async (req, res) => {
    try {
        const brand = await BrandModel.deleteOne({ _id: req.params.brandId });
        res.status(200).json({success:true, data:brand, message:'Brand successfully deleted'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

