import CarrierModel from '../../models/carriers.js';
import CountryModel from '../../models/countries.js';

export const getCarriers = async (req, res) => {
    const { page, perPage } = req.query;
    const options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(perPage, 10) || 10,
        populate: 'countryId',
    }
    try {
        const carriers = await CarrierModel.paginate( {}, options);
        res.status(200).json({ success:true, data:carriers, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const createCarrier = async (req, res) => {
    console.log("hit");
    const color = req.body;
    const newCarrier = new CarrierModel(color);
    try {
        await newCarrier.save();
        res.status(201).json({ success:true, data:newCarrier, message:'Carrier successfully created'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const getCarrierById = async (req, res) => {
    try {
        const carrier = await CarrierModel.findById(req.params.carrierId);
        res.status(200).json({ success:true, data:carrier, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const updateCarrier = async (req, res) => {
    const carrier = req.body;
    try {
        const updateCarrier = await CarrierModel.updateOne(
            { _id: req.params.carrierId },
            { $set: carrier } 
        );
        res.status(200).json({ success:true, data:updateCarrier, message:'Carrier successfully updated'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const deleteCarrier = async (req, res) => {
    try {
        const carrier = await CarrierModel.deleteOne({ _id: req.params.carrierId });
        res.status(200).json({ success:true,  data:carrier, message:'Carrier successfully deleted'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}




