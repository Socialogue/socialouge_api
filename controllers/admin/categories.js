import CategoryModel from '../../models/categories.js';
import helpers from '../../lib/helpers.js';
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

export const getCategories = async (req, res) => {
    try {
        const categories = await CategoryModel.find();
            
        return res.status(200).json({success: true, categories, message:'success'});
    } catch (error) {
        return res.status(500).json({success:false, message: error.message });
    }
}
 
export const createCategory = async (req, res) => {

    const slugItem = await helpers.makeSlug(req.body.name, 'categories');
    req.body.slug = slugItem.slug;
    req.body.slugId = slugItem.slugId;


    const category = req.body;

    const newCategory = new CategoryModel(category);
    

    try {
        await newCategory.save();

        return res.status(201).json(
            {
                success: true, 
                data:newCategory, 
                message:'Category successfully created'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getCategoryById = async (req, res) => {
    try {
        const category = await CategoryModel.findOne(
            { _id: req.params.categoryId },
            { "name": 1, "_id": 1},
        );
        return res.status(200).json(
            {
                success: true, 
                data:category, 
                message:'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const updateCategory = async (req, res) => {

    const slugItem = await helpers.updateSlug(req.body.name, 'categories', req.body.slugId);
    req.body.slug = slugItem.slug;
    req.body.slugId = slugItem.slugId;


    const category = req.body;
    try {
        const updateCategory = await CategoryModel.updateOne(
            { _id: req.params.categoryId },
            { $set: category } 
        );
        return res.status(200).json(
            { 
                success:true, 
                data:updateCategory, 
                message:'Category successfully Updated'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const deleteCategory = async (req, res) => {
    try {
        const category = await CategoryModel.deleteOne({ _id: req.params.categoryId });
        return res.status(200).json(
            {
                success: true,
                data:category, 
                message:'Category successfully Deleted'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}
 
export const catWithSubByCatId = async (req, res) => {
    try {

        const category = await CategoryModel.find(
            { _id: req.params.categoryId },
            { "name": 1, "subCategory.name":1, "subCategory._id":1},
        );

        return res.status(200).json(
            {
                success: true, 
                data: category, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
    
}







