import ColorModel from '../../models/colors.js';

export const getColors = async (req, res) => {
    const { page, perPage } = req.query;
    const options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(perPage, 10) || 10,
    }
    try {
        const colors = await ColorModel.paginate( {}, options);
        return res.status(200).json({success: true, data: colors, message:'success'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}

export const createColor = async (req, res) => {
    const color = req.body;
    const newColor = new ColorModel(color);
    try {
        await newColor.save();
        return res.status(201).json({success: true, data: newColor, message:'Color Successfully Saved'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}

export const getColorById = async (req, res) => {
    try {
        const color = await ColorModel.findById(req.params.colorId);
        return res.status(200).json({success:true, data: color, message:'success'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}

export const updateColor = async (req, res) => {
    const color = req.body;
    try {
        const updateColor = await ColorModel.updateOne(
            { _id: req.params.colorId },
            { $set: color } 
        );
        return res.status(200).json({success:true, data: updateColor, message:'success'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}

export const deleteColor = async (req, res) => {
    console.log(req.params.colorId);
    try {
        const color = await ColorModel.deleteOne({ _id: req.params.colorId });
        return res.status(200).json({ success:true, data: color, message: 'Color Successfully Deleted' });
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message});
    }
}

