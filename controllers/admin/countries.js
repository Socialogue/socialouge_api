import CountryModel from '../../models/countries.js';

export const getCountries = async (req, res) => {
    const { page, perPage } = req.query;
    const options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(perPage, 10) || 10,
    }
    try {
        // const countries = await CountryModel.find().populate('countryId');
        const countries = await CountryModel.paginate( {}, options);
        res.status(200).json({success: true, data:countries, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}   

export const createCountry = async (req, res) => {
    const country = req.body;
    const newCountry = new CountryModel(country);
    try {
        await newCountry.save();
        res.status(201).json({success: true, data:newCountry, message:'Country successfully created'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}
 
export const getCountryById = async (req, res) => {
    try {
        const country = await CountryModel.findById(req.params.countryId);
        res.status(200).json({success:true, data:country, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const updateCountry = async (req, res) => {
    const country = req.body;
    try {
        const updateCountry = await CountryModel.updateOne(
            { _id: req.params.countryId },
            { $set: country } 
        );
        res.status(200).json({success: true, data:updateCountry, message:'Country successfully Updated'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const deleteCountry = async (req, res) => {
    try {
        const country = await CountryModel.deleteOne({ _id: req.params.countryId });
        res.status(200).json({success:true, data:country, message:'Country successfully deleted'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

