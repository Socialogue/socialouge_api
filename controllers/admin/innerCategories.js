import CategoryModel from '../../models/categories.js';
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

export const createInnerCategory = async (req, res) => {
    try {
        const innerCategoryData = req.body;
        const addInnerCategory = await CategoryModel.updateOne(
            { subCategory: { $elemMatch: { _id:req.body.subCategoryId } } },
            { $push:{"subCategory.$.innerCategory":innerCategoryData}} 
        );
        return res.status(201).json({success: true, data:addInnerCategory, message:'Inner category saved!'});
    } catch (error) {
        return res.status(500).json({success:false, message: error.message });
    }
}

export const updateInnnerCategory = async (req, res) => {

    try {
        if(req.body.name){
            const innerCategoryUpdate = await CategoryModel.updateOne(
                { "subCategory.innerCategory._id": req.params.innerCategoryId },
                { $set: {"subCategory.$[].innerCategory.$.name": req.body.name} } 
            );
        }
        if(req.body.sizes){
            const innerCategoryUpdate = await CategoryModel.updateOne(
                { "subCategory.innerCategory._id": req.params.innerCategoryId },
                { $set: {"subCategory.$[].innerCategory.$.sizes": req.body.sizes} } 
            );
        }
        if(req.body.color){
            const innerCategoryUpdate = await CategoryModel.updateOne(
                { "subCategory.innerCategory._id": req.params.innerCategoryId },
                { $set: {"subCategory.$[].innerCategory.$.color": req.body.color} } 
            );
        }
        
        return res.status(200).json(
            { 
                success:true, 
                message:'Inner Category successfully Updated'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getInnerCategoryById = async (req, res) => {
    
    try {
        const innerCategory = await CategoryModel.aggregate([
            { 
                $match: {
                    "subCategory.innerCategory._id": ObjectId(req.params.innerCategoryId) 
                }, 
            },
            { $unwind: "$subCategory" },
            { $unwind : "$subCategory.innerCategory" },
            { 
                $match: {
                    "subCategory.innerCategory._id": ObjectId(req.params.innerCategoryId) 
                }, 
            },
            { 
                $project : { 
                    "_id":1, 
                    "name": 1, 
                    "subCategory.name" :1, 
                    "subCategory._id" :1,
                    "subCategory.innerCategory.name" :1, 
                    "subCategory.innerCategory._id" :1  
                } 
            }
            
        ])
        return res.status(200).json(
            {
                success: true, 
                data: innerCategory, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
    
}

export const innerCat2ByInnerCatId = async (req, res) => {
    try {
        const innerCategory = await CategoryModel.aggregate([
            { 
                $match: {
                    "subCategory.innerCategory._id": ObjectId(req.params.innerCategoryId) 
                }, 
            },
            { $unwind : "$subCategory" },
            { 
                $match: {
                    "subCategory.innerCategory._id": ObjectId(req.params.innerCategoryId) 
                }, 
            },
            { 
                $project : 
                { 
                    "name": 1, 
                    "subCategory.name" : 1, 
                    "subCategory._id" : 1,
                    "subCategory.innerCategory._id" : 1,
                    "subCategory.innerCategory.name" : 1,
                    "subCategory.innerCategory.innerCategory2._id" : 1,
                    "subCategory.innerCategory.innerCategory2.name" : 1,
                } 
            }
            
        ])
        return res.status(200).json(
            { 
                success: true, 
                data: innerCategory, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
    
}

export const innerCategoryDelete = async (req, res) => {
    try {
        const deleteInnerCategory = await CategoryModel.findOneAndUpdate(
            { "subCategory._id": req.body.subCategoryId },
            { "$pull": {"subCategory.$.innerCategory":{ _id: req.params.innerCategoryId } } }
        );
        return res.status(200).json(
            {
                success: true, 
                data: deleteInnerCategory, 
                message:'Inner category deleted!'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}