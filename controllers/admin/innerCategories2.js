import CategoryModel from '../../models/categories.js';
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

export const createInnerCategory2 = async (req, res) => {
    const innerCategory2Data = req.body;
    try {
        const addInnerCategory2 = await CategoryModel.updateOne(
            { "subCategory.innerCategory._id": req.body.innerCategoryId },
            { "$push": {"subCategory.$[].innerCategory.$.innerCategory2":innerCategory2Data } }
        );
        return res.status(201).json({success: true, data:addInnerCategory2, message:'Inner category saved!'});
    } catch (error) {
        return res.status(500).json({success:false, message: error.message });
    }
}

export const updateInnerCategory2 = async (req, res) => {
    console.log(req.body.name);
    const innerCat2Data = req.body;
    try {
        const innerCategory2Update = await CategoryModel.updateOne(
            { "subCategory.innerCategory.innerCategory2._id": req.params.innerCategory2Id },
            { "$set": {"subCategory.$[].innerCategory.$[].innerCategory2.$.name":req.body.name} },
            { upsert: true }

            // { $set: { 'subCategory.$[].innerCategory.$[].innerCategory2.$[i].name': 'Baz' } },
            // { arrayFilters: [{ 'i._id': mongoose.Types.ObjectId(req.params.innerCategory2Id) }] }


        );

        // const innerCategory2Update = await CategoryModel.findOne(
        //     { "subCategory.innerCategory.innerCategory2._id": req.params.innerCategory2Id },
        //     { "$set": {"subCategory.$[].innerCategory.$.innerCategory2.name":req.body.name } }
        // );

        return res.status(200).json(
            {
                success: true,
                data:innerCategory2Update, 
                message:'Inner category2 Updated!'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getInnerCategory2ById = async (req, res) => {
    
    try {
        const innerCategory2 = await CategoryModel.aggregate([
            { 
                $match: {
                    "subCategory.innerCategory.innerCategory2._id": ObjectId(req.params.innerCategory2Id) 
                }, 
            },
            { $unwind: "$subCategory" },
            { $unwind : "$subCategory.innerCategory" },
            { $unwind : "$subCategory.innerCategory.innerCategory2" },
            { 
                $match: {
                    "subCategory.innerCategory.innerCategory2._id": ObjectId(req.params.innerCategory2Id) 
                }, 
            },
            { 
                $project : { 
                    "_id":1, 
                    "name": 1, 
                    "subCategory.name" :1, 
                    "subCategory._id" :1,
                    "subCategory.innerCategory.name" :1, 
                    "subCategory.innerCategory._id" :1 , 
                    "subCategory.innerCategory.innerCategory2.name" :1, 
                    "subCategory.innerCategory.innerCategory2._id" :1 
                } 
            }
            
        ])
        return res.status(200).json(
            {
                success: true, 
                data: innerCategory2, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
    
}

export const innerCategory2Delete = async (req, res) => {

    try {
        const deleteInnerCategory2 = await CategoryModel.findOneAndUpdate(
            { "subCategory.innerCategory._id": req.body.innerCategoryId },
            { "$pull": {"subCategory.$[].innerCategory.$.innerCategory2":{ _id: req.params.innerCategory2Id } } }
        );
        return res.status(200).json(
            {
                success: true, 
                data: deleteInnerCategory2, 
                message:'Inner category 2 deleted!'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}
