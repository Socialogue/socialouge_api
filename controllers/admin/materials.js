import MaterialModel from '../../models/materials.js';

export const getMaterials = async (req, res) => {
    const { page, perPage } = req.query;
    const options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(perPage, 10) || 10,
    }
    try {
        const materials = await MaterialModel.paginate( {}, options);
        res.status(200).json({success:true, data:materials, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const createMaterial = async (req, res) => {
    const material = req.body;
    const newMaterial = new MaterialModel(material);
    try {
        await newMaterial.save();
        res.status(201).json({success:true, data:newMaterial, message:'Material successfully created'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const getMaterialById = async (req, res) => {
    try {
        const material = await MaterialModel.findById(req.params.materialId);
        res.status(200).json({success:true, data:material, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const updateMaterial = async (req, res) => {
    const material = req.body;
    try {
        const updateMaterial = await MaterialModel.updateOne(
            { _id: req.params.materialId },
            { $set: material } 
        );
        res.status(200).json({success:true, data:updateMaterial, message:'Material successfully updated'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const deleteMaterial = async (req, res) => {
    try {
        const material = await MaterialModel.deleteOne({ _id: req.params.materialId });
        res.status(200).json({ success:true, data:material, message:'Material successfully deleted'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

