import SeasonModel from '../../models/seasons.js';

export const getSeasons = async (req, res) => {
    const { page, perPage } = req.query;
    const options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(perPage, 10) || 10,
    }
    try {
        const seasons = await SeasonModel.paginate( {}, options);
        res.status(200).json({success: true, data:seasons, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const createSeason = async (req, res) => {

    const season = req.body;
    const newSeason = new SeasonModel(season);
    try {
        await newSeason.save();
        res.status(201).json({success: true, data: newSeason, message:'Season successfully created'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const getSeasonById = async (req, res) => {
    try {
        const season = await SeasonModel.findById(req.params.seasonId);
        res.status(200).json({success:false, data:season, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const updateSeason = async (req, res) => {
    const season = req.body;
    try {
        const updateSeason = await SeasonModel.updateOne(
            { _id: req.params.seasonId },
            { $set: season } 
        );
        res.status(200).json({success: true, data: updateSeason, message:'Season successfully updated'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const deleteSeason = async (req, res) => {
    try {
        const season = await SeasonModel.deleteOne({ _id: req.params.seasonId });
        res.status(200).json({success: true, data: season, message:'Season successfully deleted'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

