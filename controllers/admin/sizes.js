import SizeModel from '../../models/sizes.js';

export const getSizes = async (req, res) => {
    try {
        const sizes = await SizeModel.find();
        res.status(200).json(sizes);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const getSizeByCategory = async (req, res) => {
    try {
        const sizes = await SizeModel.find({category:req.params.categoryId});
        res.status(200).json(sizes);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}


export const createSize = async (req, res) => {
    const size = req.body;
    const newSize = new SizeModel(size);
    try {
        await newSize.save();
        res.status(201).json(newSize);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const getSizeById = async (req, res) => {
    try {
        const size = await SizeModel.findById(req.params.sizeId);
        res.status(200).json(size);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const updateSize = async (req, res) => {
    const size = req.body;
    try {
        const updateSize = await SizeModel.updateOne(
            { _id: req.params.sizeId },
            { $set: size } 
        );
        res.status(200).json(updateSize);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const deleteSize = async (req, res) => {
    try {
        const size = await SizeModel.remove({ _id: req.params.sizeId });
        res.status(200).json(size);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

