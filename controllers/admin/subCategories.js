import CategoryModel from '../../models/categories.js';
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

export const createSubCategory = async (req, res) => {
    try {
        const subCategoryData = req.body;
        const addSubCategory = await CategoryModel.findOneAndUpdate(
            {_id: req.body.categoryId},
            { $push: { subCategory: subCategoryData  } }
        );
        return res.status(201).json({success: true, data: addSubCategory, message: 'Sub Category successfully saved!'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}
export const updateSubCategory = async (req, res) => {
    console.log(req.body.color);
    const subCategory = req.body;
    try {
        if(req.body.name){
            const subCategoryUpdate = await CategoryModel.updateOne(
                { "subCategory._id": req.params.subCategoryId },
                { $set: {"subCategory.$.name": req.body.name} } 
            );
        }
        if(req.body.sizes){
            const subCategoryUpdate = await CategoryModel.updateOne(
                { "subCategory._id": req.params.subCategoryId },
                { $set: {"subCategory.$.sizes": req.body.sizes} } 
            );
        }
        if(req.body.color){
            const subCategoryUpdate = await CategoryModel.updateOne(
                { "subCategory._id": req.params.subCategoryId },
                { $set: {"subCategory.$.color": req.body.color} } 
            );
        }
        
        
        return res.status(200).json(
            { 
                success:true, 
                message:'Sub Category successfully Updated'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getSubCategoryById = async (req, res) => {
    try {
        const subCategory = await CategoryModel.aggregate([
            { 
                $match: {
                    "subCategory._id": ObjectId(req.params.subCategoryId) 
                }, 
            },
            { $unwind : "$subCategory" },
            { 
                $match: {
                    "subCategory._id": ObjectId(req.params.subCategoryId) 
                }, 
            },
            { $project : { "_id":1, "name": 1, "subCategory.name" :1, "subCategory._id" :1 } }
            
        ])
        return res.status(200).json(
            {
                success: true, 
                data: subCategory, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
    
}


export const deleteSubCategory = async (req, res) => {
    try {
        const deleteSubCategory = await CategoryModel.findOneAndUpdate(
            { _id: req.body.categoryId },
            { $pull: { subCategory: { _id: req.params.subCategoryId }  } } 
        );
        return res.status(200).json(
            { 
                success: true, 
                data: deleteSubCategory, 
                message: 'Sub Category successfully Deleted!'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}


export const innerCatBySubCatId = async (req, res) => {
    try {
        const subCategory = await CategoryModel.aggregate([
            { 
                $match: {
                    "subCategory._id": ObjectId(req.params.subCategoryId) 
                }, 
            },
            { $unwind : "$subCategory" },
            { 
                $match: {
                    "subCategory._id": ObjectId(req.params.subCategoryId) 
                }, 
            },
            { 
                $project : 
                { 
                    "name": 1, 
                    "subCategory.name" : 1, 
                    "subCategory._id" : 1,
                    "subCategory.innerCategory._id" : 1,
                    "subCategory.innerCategory.name" : 1,
                } 
            }
            
        ])
        return res.status(200).json(
            { 
                success: true, 
                data: subCategory, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
    
}