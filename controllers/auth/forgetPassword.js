import { createRequire } from 'module';
const require = createRequire(import.meta.url);
import UserModel from '../../models/users.js';
import VerificationModel from '../../models/verifications.js';
import mailSetting from '../../lib/mailSetting.js'; 
import hbs from 'nodemailer-express-handlebars';
import { sendConfirmationMail, sendConfirmationToPhone, saveOtp } from './register.js';

export const forgetPassword = async (req, res) => {
    const user = await UserModel.findOne({$or: [
        {email: req.body.username},
        {phone: req.body.username}
    ]});
    if(!user){
        return res.status(404).json(
            {
                success:false, 
                message: 'Invalid Username!' 
            }
        );
    }
    try {
        const otp = await saveOtp(req.body.username);
        if(req.body.accountType == 1){
            const sendMail = await sendConfirmationMail(user, otp)
            const sendableData = {
                username: req.body.username,
                accountType: 1
            }
            return res.status(200).json(
                { 
                    success: true, 
                    data:sendableData, 
                    message: 'Opt has been sent to you Email' 
                }
            ); 
        }else{
            const sendSms = await sendConfirmationToPhone( user, otp)
            if(sendSms){

                const sendableData = {
                    username:req.body.username,
                    accountType:req.body.accountType
                }
                return res.status(200).json(
                    { 
                        success: true, 
                        data:sendableData, 
                        message: 'Otp has been sent to you phone' 
                    }
                ); 
            }else{
                return res.status(500).json(
                    { 
                        success: false, 
                        message: 'Sms server error' 
                    }
                ); 
            }

        }
    } catch (error) {
        return res.status(500).json(
            { 
                success: false,
                message: error.message 
            }
        );
    }
    
}

export const matchForgotPasswordOtp = async (req, res) => {

    const user = await VerificationModel.findOne(
        { username: req.body.username }
    );
    if(!user){
        return res.status(400).json(
            {
                success:false, 
                message: 'Invalid OTP' 
            }
        );
    }
    
    if(user.otp == req.body.otp){
        const deleteOtp = await VerificationModel.deleteOne(
            { username:req.body.username}
        );
        return res.status(200).json(
            {
                success: true, 
                message:'Otp successfully match!'
            }
        );
    }else{
        return res.status(400).json(
            {
                success: false, 
                message: 'Invalid OTP' 
            }
        );
    }
    return res.status(500).json(
        {
            success:false, 
            message: 'Invalid OTP' 
        }
    );

}

async function sendResetPasswordLink(user, resetLink){
    const transporter = await mailSetting.transporter();
    const handlebarOptions = await mailSetting.handlebarOptions();
    transporter.use("compile",hbs(handlebarOptions));
    const mailOptions = {
        from: "Socialogue Team ✔ <foo@blurdybloop.com>",
        to: user.email,
        subject: 'Reset Password',
        text: '',
        template: 'forgetPassword',
        context: {
            link: resetLink
        }
    };
    transporter.sendMail(mailOptions, (error, info) => {});
}