import UserModel from '../../models/users.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { sendConfirmationMail, sendConfirmationToPhone, saveOtp } from './register.js';

export const login = async (req, res) => {
    if(req.body.accountType == 1 || req.body.accountType == 2){
        return await directLogin(req, res);
    }else if(req.body.accountType == 3 || req.body.accountType == 4){
        return await socialLogin(req, res);
    }else{
        return res.status(400).json({
            success: false,
            message: 'Account Type is Not Define'
        });
    }
}

async function directLogin(req, res){

    const user = await UserModel.findOne({$or: [
        {email: req.body.username},
        {phone: req.body.username}
    ]});
    if(!user){
        return res.status(401).json({
            success: false,
            message: 'Invalid username or password'
        });
    }
    if(user.verified == false){
        const otp = await saveOtp(user.email);
        if(user.accountType == 1){
            const sendMail = await sendConfirmationMail(user, otp);
            const sendableData = {
                username: user.email,
                accountType: 1
            }
            return res.status(200).json({
                success: true,
                message: 'OTP code sent to your email',
                data: sendableData,
                needToVerify: true
            });

        }else if( user.accountType == 2){
            const sendSms = await sendConfirmationToPhone(user, otp);
            const sendableData = {
                username: user.phone,
                accountType: 2
            }
            return res.status(200).json({
                success: true,
                message: 'OTP code sent to your phone',
                data: endableData,
                needToVerify: true
            });
        }
    }

    bcrypt.compare(req.body.password, user.password, (err, result) => {
        if(err){
            return res.status(401).json({
                success: false,
                message: 'Invalid username or password'
            });
        }
        if(result){
            const token = jwt.sign(
                {
                    userId: user._id
                },
                process.env.JWT_KEY,
                {
                    expiresIn: "1d"
                }
            );
            return res.status(200).json(
                {
                    success: true,
                    message:'Authentication Success!',
                    data: user,
                    token:token
                }
            );
        }
        return res.status(401).json({
            success: false,
            message:'Invalid username or password'
        });

    })
}

async function socialLogin(req, res){
    let user = await UserModel.findOne(
        { email: req.body.email }
    );
    req.body.verified = true;
    const userData = req.body;
    if(!user){
        const newUser = new UserModel(userData);
        await newUser.save();
        user = newUser;
    }
    const token = jwt.sign(
        {
            userId: user._id
        },
        process.env.JWT_KEY,
        {
            expiresIn: "1h"
        }
    );
    return res.status(200).json(
        {
            success: true,
            message:'Authentication Success!',
            data: user,
            token:token
        }
    );

}
