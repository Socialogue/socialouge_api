import { createRequire } from 'module';
const require = createRequire(import.meta.url);

import UserModel from '../../models/users.js';
import VerificationModel from '../../models/verifications.js';
import mailSetting from '../../lib/mailSetting.js';
import hashPassword from '../../lib/hashPassword.js';
import hbs from 'nodemailer-express-handlebars';
import { v4 as uuidv4 } from 'uuid';
import helpers from '../../lib/helpers.js';

export const register = async (req, res) => {
    if(req.body.accountType == 1){
       return await emailRegister(req, res);
    }else if(req.body.accountType == 2){
        return await phoneRegister(req, res);
    }else{
        res.status(404).json(
            { 
                success: false, 
                message: 'Account Type is Not Define'
            }
        );
    }
}
async function emailRegister(req, res){
    try {
        
        const findUserByEmail = await UserModel.findOne(
            { email: req.body.username}
        );

        if(findUserByEmail){
            if(findUserByEmail.verified == true){
                const AccountType = await accountType(findUserByEmail);
                return res.status(400).json(
                    {   
                        success:false, 
                        message:`You have already account with ${AccountType}`
                    }
                );
            }else{
                const otp = await saveOtp( req.body.username );
                await sendConfirmationMail(findUserByEmail, otp);
                const sendableData = {
                    username:req.body.username,
                    accountType:req.body.accountType
                }
                return res.status(200).json(
                    { 
                        success: true, 
                        data:sendableData, 
                        message: 'Confirmation OPT has been sent your email. Please verify your acccount'
                    }
                );

            }
        }
        const passwodHassed = await hashPassword.getHashedPassword(req.body.password);
        req.body.password = passwodHassed;
        let user = req.body;
        req.body.email = req.body.username;

        const slugItem = await helpers.makeSlug(user.fullName, 'users');
        user.slug = slugItem.slug;
        user.slugId = slugItem.slugId;

        const newUser = new UserModel(user);
        await newUser.save();
        const otp = await saveOtp( req.body.username );
        await sendConfirmationMail(newUser, otp);
        const otpData = {
            username: newUser.email,
            otp: otp
        }
        const saveOtpToVeri = new VerificationModel(otpData);
        await saveOtpToVeri.save();
        const sendableData = {
            username:req.body.username,
            accountType:req.body.accountType
        }
        return res.status(200).json(
            { 
                success: true, 
                data:sendableData, 
                message: 'Confirmation OPT has been sent your email. Please verify your acccount'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success: false, 
                message: error.message 
            }
        );
    }
}
async function phoneRegister(req, res){
    try {
        const findUserByPhone = await UserModel.findOne(
            { phone: req.body.username }
        );
        if(findUserByPhone){
            if(findUserByPhone.verified == true){
                const AccountType = await accountType(findUserByPhone);
                return res.status(400).json(
                    {   
                        success:false, 
                        message:`You have already account with ${AccountType}`
                    }
                );
            }else{
                const otp = await saveOtp( req.body.username );
                const sendToPhone = await sendConfirmationToPhone(findUserByPhone, otp);
                if(sendToPhone){
                    const sendableData = {
                        username:req.body.username,
                        accountType:req.body.accountType
                    }
                    return res.status(200).json(
                        { 
                            success: true, 
                            data:sendableData, 
                            message: 'Confirmation OPT has been sent your phone. Please verify your acccount'
                        }
                    );
                }
                return res.status(500).json(
                    { 
                        success: false, 
                        message: 'Something worng with sms server'
                    }
                );
            }
        }
        const passwodHassed = await hashPassword.getHashedPassword(req.body.password);
        req.body.password = passwodHassed;
        req.body.phone = req.body.username;
        let user = req.body;
        const slugItem = await helpers.makeSlug(user.fullName, 'users');
        user.slug = slugItem.slug;
        user.slugId = slugItem.slugId;

        const newUser = new UserModel(user);
        await newUser.save();
        const otp = await saveOtp( req.body.username );
        const sendToPhone = await sendConfirmationToPhone(newUser, otp);
        if(sendToPhone){
            const sendableData = {
                username:req.body.username,
                accountType:req.body.accountType
            }
            return res.status(200).json(
                { 
                    success: true, 
                    data:sendableData, 
                    message: 'Confirmation OPT has been sent your phone. Please verify your acccount'
                }
            );
        }
        return res.status(500).json(
            { 
                success: false, 
                message: 'Something worng with sms server'
            }
        ); 
    } catch (error) {
        return res.status(500).json(
            { 
                success: false, 
                message: error.message 
            }
        );
    }
    
}

export const resendCode = async (req, res) => {
    try {
        if( req.body.accountType == 1){
            const findUserByEmail = await UserModel.findOne(
                { email: req.body.username}
            );
            const otp = await saveOtp( req.body.username );
            await sendConfirmationMail(findUserByEmail, otp);
        }else if(req.body.accountType == 2){
            const findUserByPhone = await UserModel.findOne(
                { phone: req.body.username}
            );
            const otp = await saveOtp( req.body.username );
            const sendToPhone = await sendConfirmationToPhone(findUserByPhone, otp);
        }
        return res.status(200).json(
            { 
                success: true, 
                message: 'Confirmation OPT has been sent your email. Please verify your acccount'
            }
        );

    } catch (error) {
        return res.status(500).json(
            { 
                success: false, 
                message: error.message 
            }
        );
    }
}

export const sendConfirmationMail = async (user, confirmationLink) => {
    const transporter = await mailSetting.transporter();
    const handlebarOptions = await mailSetting.handlebarOptions();
    transporter.use("compile",hbs(handlebarOptions));
    const mailOptions = {
        from: "Socialogue Team ✔ <foo@blurdybloop.com>",
        to: user.email,
        subject: 'Confirmation Mail',
        text: '',
        template: 'signupConfirmation',
        context: {
            link: confirmationLink
        }
    };
    transporter.sendMail(mailOptions, (error, info) => {});
}

export const sendConfirmationToPhone = async (user, otp) => {
    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken = process.env.TWILIO_AUTH_TOKEN;
    const client = require('twilio')(accountSid, authToken);
    try {
        const sendSms = await client.messages.create(
            {
                body: `Your socialogue Verification Code is: ${otp}`,
                from: '+17378885229',
                to: '+8801909888251'
            }
        )
        return true;
    } catch (error) {
        return false;
    }
}

export const saveOtp = async ( username ) => {
    try {
        const otp = Math.floor(Math.random() * 90000) + 10000;
        const isUserExist = await VerificationModel.findOne(
            { username: username}
        )
        if(isUserExist){
            const updateOtp = await VerificationModel.updateOne(
                { username: username },
                { $set: { otp: otp} } 
            );
            
        }else{
            const otpObject = {
                username: username,
                otp: otp
            }
            const saveOtp = new VerificationModel(otpObject);
            await saveOtp.save();
        }
        return otp;

    } catch (error) {
        console.log(error);
        return 'OPT Save Fail';
    }
}

export const accountType = async ( user ) => {
    var userAccountType = '';
    if(user.accountType == 1){
        userAccountType = 'Gmail';
    }else if(user.accountType == 2){
        userAccountType = 'Phone';
    }else if(user.accountType == 3){
        userAccountType = 'Facebook';
    }else if(user.accountType == 3){
        userAccountType = 'Google';
    }
    return userAccountType;
}
