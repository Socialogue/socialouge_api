import UserModel from '../../models/users.js';
import hashPassword from '../../lib/hashPassword.js';
import VerificationModel from '../../models/verifications.js';
import jwt from 'jsonwebtoken';

export const resetPassword = async (req, res) => {
    try {
        let user = await UserModel.findOne({$or: [
            {email: req.body.username},
            {phone: req.body.username}
        ]});
        console.log(user)

        if(!user){
            return res.status(500).json({success: false, message:'Reset Password Fail'});
        }
        if(req.body.password != req.body.repeatPassword){
            return res.status(500).json({success: false, message:'Password not match with confirm password'});
        }
        const passwodHassed = await hashPassword.getHashedPassword(req.body.password);
        user.password = passwodHassed;
        await user.save();
        const token = jwt.sign(
            {
                userId: user._id
            }, 
            process.env.JWT_KEY,
            {
                expiresIn: "1h"
            }
        );
        return res.status(200).json({success: true, message:'Authentication Success!', data: user, token:token});

    } catch (error) {
        res.status(404).json({success:false, message: error.message });
    }
    
}