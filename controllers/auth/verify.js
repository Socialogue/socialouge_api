import UserModel from '../../models/users.js';
import VerificationModel from '../../models/verifications.js';
import jwt from 'jsonwebtoken';

export const verify = async (req, res) => {
    try {
        const user = await VerificationModel.findOne(
            { username:req.body.username }
        );

        if(!user){
            return res.status(401).json(
                {
                    success:false, 
                    message: 'Invalid Verification Code!' 
                }
            );
        }
        if(req.body.otp != user.otp){
            return res.status(401).json(
                {
                    success:false, 
                    message: 'Invalid Verification Code!' 
                }
            );
        }

        let updateUser;
        let getCurrentUser;
        if(req.body.accountType == 1){
            updateUser = await UserModel.updateOne(
                { email: user.username },
                { verified: true } 
            );
            getCurrentUser = await UserModel.findOne(
                { email: user.username}
            )
        }else{
            updateUser = await UserModel.updateOne(
                { phone: user.username },
                { verified: true } 
            );
            getCurrentUser = await UserModel.findOne(
                { phone: user.username}
            )
        }
        
        const deleteToken = await VerificationModel.remove(
            { username:req.body.username }
        );
        const token = jwt.sign(
            {
                userId: updateUser._id
            }, 
            process.env.JWT_KEY,
            {
                expiresIn: "1h"
            }
        );
        return res.status(200).json(
            {
                success: true, 
                data: getCurrentUser, 
                token:token, 
                message:'Authentication Success!'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success: false,
                message: error.message 
            }
        );
    }
}