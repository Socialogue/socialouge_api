import CartModel from '../../models/cart.js';

export const getCart = async (req, res) => {
    const clientId = req.body.authUserId;
    try {
        const carts = await CartModel.find(
            { clientId: clientId }
        )
        return res.status(200).json(
            {
                success:true,
                data: carts, 
                message:'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const addToCart = async (req, res) => {
    const carts = req.body;
    
    const newCart = new CartModel(carts);
    try {
        await newCart.save();
        return res.status(201).json(
            {
                success:true, 
                message:'Product Successfully added to Cart'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const udateCart = async (req, res) => {
    const carts = req.body;
    try {
        const updateCart = await CartModel.updateOne(
            { clientId: req.body.clientId },
            { $set: req.body } 
        );
        return res.status(200).json(
            {
                success:true, 
                message:'Cart successfully updated'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    } 
}

export const deleteFromCart = async (req, res) => {
    try {
        const deleteItem = await CartModel.deleteOne(
            { clientId: req.body.clientId },
        );
        return res.status(200).json(
            { 
                success: true, 
                data: deleteItem, 
                message: 'Cart has been deleted'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}