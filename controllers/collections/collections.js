import UserModel from '../../models/users.js';
import ProductModel from '../../models/products.js';
import mongoose from 'mongoose';

export const createCollection = async (req, res) => {
    const collectionId = mongoose.Types.ObjectId();
    req.body._id = collectionId;
    try {
        const userId = req.body.authUserId;
        const addCollection = await UserModel.updateOne(
            {_id: userId},
            { $push: { collections: req.body  } }
        );
        const getCollection = await UserModel.aggregate([
            { 
                $match: {
                    "collections._id": collectionId
                }, 
            },
            { $unwind: "$collections" },
            { 
                $match: {
                    "collections._id": collectionId
                }, 
            },
            { 
                $project : { 
                    "collections":1, 
                } 
            }
            
        ])
        const collectionObj = getCollection[0].collections;
        return res.status(200).json(
            { 
                success:true,
                data: collectionObj,
                message:'Collection created successfully'
            }
    );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getAllCollection = async (req, res) => {
    const userId = req.body.authUserId;
    try {
        var allCollection = await UserModel.findOne(
            { _id: userId },
            { collections: 1}
        )

        return res.status(200).json(
            { 
                success:true,
                data: allCollection,
                message:'success'
            }
    );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}


export const getCollection = async (req, res) => {
    const userId = req.body.authUserId;
    try {
        var allCollection = await UserModel.findOne(
            { _id: userId },
            { collections: 1}
        )

        var finalProducts = [];

        for( let collection of allCollection.collections){
            var demoCollection = collection;
            var prodArr = collection.products;
            const prodArrLength = prodArr.length;
            var products;
            if(prodArrLength > 3){
                prodArr = prodArr.slice(Math.max(prodArr.length - 4, 0));
            }
            products =  await ProductModel.find(
                { "_id": { "$in": prodArr } },
                { mainImage: 1 }
            )
            demoCollection.products = products;
            
            finalProducts.push(demoCollection);
        }
        
        return res.status(200).json(
            { 
                success:true,
                data: finalProducts,
                message:'success'
            }
    );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const addProduct = async (req, res) => {
    try {
        const collectionId = req.body.collectionId;
        const addProduct = await UserModel.updateOne(
            {"collections._id": collectionId},
            { $push: { "collections.$.products": req.body.productId  } , $inc: { "collections.$.totalProduct": 1 } }
        );

        return res.status(200).json(
            { 
                success:true,
                data: addProduct,
                message:'Product added successfully'
            }
    );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const deleteProduct = async (req, res) => {
    try {
        const collectionId = req.body.collectionId;
        const addProduct = await UserModel.updateOne(
            {"collections._id": collectionId},
            { $pull: { "collections.$.products": req.body.productId  }, $inc: { "collections.$.totalProduct": -1 } }
        );

        return res.status(200).json(
            { 
                success:true,
                data: addProduct,
                message:'Product Deleted successfully'
            }
    );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const specificProduct = async (req, res) => {
    try {
        const collectionId = mongoose.Types.ObjectId(req.body.collectionId);
        const getCollection = await UserModel.aggregate([
            { 
                $match: {
                    "collections._id": collectionId
                }, 
            },
            { $unwind: "$collections" },
            { 
                $match: {
                    "collections._id": collectionId
                }, 
            },
            { 
                $project : { 
                    "collections":1, 
                } 
            }
            
        ])

        const productIds = getCollection[0].collections.products;
        const products =  await ProductModel.find(
            { "_id": { "$in": productIds } }
        )
          
        return res.status(200).json(
            { 
                success:true,
                data: products,
                message:'success'
            }
    );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}



