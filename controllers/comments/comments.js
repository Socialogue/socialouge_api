import commentModel from '../../models/comments.js';
import PostModel from '../../models/posts.js'

export const getComment = async (req, res) => {
    const postId = req.query.postId;
    const skip = parseInt(req.query.skip, 10) || 0;
    const limit = parseInt(req.query.limit, 10) || 10;
    try {
        
        const comments = await commentModel.find(
            { post: postId },
            { 
                replay: { $slice:[0, 2] } 
            }
        ).populate(
            [
                {
                    path: "user",
                    select:"fullName slug profileImage"
                },
                {
                    path: "replay.user",
                    select:"fullName slug profileImage"
                },
                {
                    path: "replay.mention",
                    select:"fullName slug profileImage"
                }

            ]
        )
        .skip(skip).limit(limit)

        const totalComment = await PostModel.findOne(

            { _id: postId },
            { totalComment: 1 }
        );
    
        return res.status(200).json(
            {
                success: true, 
                data:comments,
                totalComment: totalComment.totalComment,
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getReply = async (req, res) => {
    const commentId = req.query.commentId;
    const skip = parseInt(req.query.skip, 10) || 0;
    const limit = parseInt(req.query.limit, 10) || 10;
    try {


        const replies = await commentModel.findOne(
            { _id: commentId },
            { 
                replay: { $slice:[skip, limit] } 
            }
        ).populate(
            [
                {
                    path: "replay.user",
                    select: 'fullName slug profileImage',
                },

                {
                    path: "replay.mention",
                    select: 'fullName slug profileImage',
                },

            ]
        )
    
        return res.status(200).json(
            {
                success: true, 
                data: replies,
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}