import DeliveryMethodModel from '../../models/deliveryMethod.js';

export const getDeliveryMethod = async (req, res) => {
    try {
        const deliveryMethods = await DeliveryMethodModel.find();
        res.status(200).json(
            {
                success:true, 
                data:deliveryMethods, 
                message:'success'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const addDeliveryMethod = async (req, res) => {
    const deliveryInfo = req.body;
    const newDelivery = new DeliveryMethodModel(deliveryInfo);
    try {
        await newDelivery.save();
        res.status(200).json(
            {
                success:true, 
                message:'Delivery method successfully added'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}
export const updateDeliveryMethod = async (req, res) => {
    try {
        const updateDelivery = await DeliveryMethodModel.updateOne(
            { _id: req.params.id },
            { $set: req.body } 
        );
        res.status(200).json(
            {
                success:true, 
                message:'Delivery method successfully updated'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const deleteDeliveryMethod = async (req, res) => {
    try {
        const deleteDelivery = await DeliveryMethodModel.deleteOne({ _id: req.params.id });
        res.status(200).json(
            {
                success:true, 
                data: deleteDelivery, 
                message:'Delivery method successfully deleted'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}