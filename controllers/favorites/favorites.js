import UserModel from '../../models/users.js';

export const createFavorite = async (req, res) => {
    try {
        console.log(req.body.userId, req.body.productId);
        const productId = req.body.productId;
        let message = '';
        let checkItem = await UserModel.findOne(
            { _id: req.body.userId,"favoriteProduct.productId": productId}
        )
        
        if(!checkItem){
            const addFavorite = await UserModel.findOneAndUpdate(
                {_id: req.body.userId},
                { $push: { favoriteProduct: { productId: productId }  }, $inc: { TotalfavoriteProduct: 1 } }
            );
            message = 'Product Favorites';
        }else{
            const removeFavorite = await UserModel.findOneAndUpdate(
                {_id: req.body.userId},
                { $pull: { favoriteProduct: { productId: productId }  }, $inc: { TotalfavoriteProduct: -1 } }
            );
            message = 'Product removed from Favorites';
        }
        
        return res.status(200).json(
            { 
                success:true, 
                message: message
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const removeFavorite = async (req, res) => {
    try {
        const productId = req.body.productId;

        const deleteFromFavorite = await UserModel.findOneAndUpdate(
            {_id: req.body.userId},
            { $pull: { favoriteProduct: { productId: productId }  } }
        );
        return res.status(200).json({ success:true, data:deleteFromFavorite, message:'Favorite successfully removed'});
    } catch (error) {
        return res.status(400).json({ success:false, message: error.message });
    }
} 
export const getFavoriteProduct = async (req, res) => {
    try {
        const skip = parseInt(req.query.skip, 10) || 0;
        const limit = parseInt(req.query.limit, 10) || 10;

        let product = await UserModel.findOne(
            { _id: req.query.userId },
            { 
                name: 1, 
                favoriteProduct: { $slice:[skip, limit] } 
            }
            
        ) .populate(
            {
                path: "favoriteProduct.productId",
                select: '_id createdAt mainImage variations title slug userId shopId',
                populate : (
                    [
                        {
                            path : 'userId',
                            select: '_id fullName',
                        },
                        {
                            path : 'shopId',
                            select: '_id shopName',
                        }
                    ]
                ), 
            }
        );
        let totalItem = 0; 
        

        let countTotalItem = await UserModel.findOne(
            { _id: req.query.userId },
        );
        
        totalItem = countTotalItem.favoriteProduct.length;

        return res.status(200).json(
            {
                success:true, 
                data:product, 
                totalItem: totalItem,
                message:'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}