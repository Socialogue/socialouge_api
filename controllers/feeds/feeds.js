import ProductModel from '../../models/products.js';
import PostModel from '../../models/posts.js';


export const getProductAndPosts = async (req, res) => {
    try {

        const product = await ProductModel.find(
            {}
        ).populate('userId shopId').limit(20).sort({createdAt: -1});
		
        const posts  =  await PostModel.find(
        	{},
			// { 
            //     "comments.replay": { $slice:[0, 2] } 
            // }
        ).populate(
        	[
	        	{
	        		path: "userId",
	        		select:"totalLikedPost fullName profileImage"
	        	},
	        	{
	        		path: "tags.productId",
	        		select:"userId shopId title slug mainImage variations"
	        	},
	        	{
	        		path: "comments",
					// "comments.replay": { $size: 1 },
					replay: { $slice: 1 },
	        		options: {
				        limit: 2,
				        skip: 0,
						// "comments.replay": { $limit:2 },
						
				    },
					 

	        		populate : (
	                    [
	                        {
	                            path : 'user',
	                            select: 'fullName slug profileImage',
	                        },

	                        {
	                            path : 'replay.user',
	                            select: 'fullName slug profileImage',
	                        },

	                        {
	                            path : 'replay.mention',
	                            select: 'fullName slug profileImage',
	                        },

	                    ]
	                )
	        	}

        	]
        ).limit(20).sort({createdAt: -1});



        return res.status(200).json(
        	{
        		success:true, 
        		// products:product, 
        		posts:posts, 
        		message:'success'
        	}
        );


    } catch (error) {

        return res.status(500).json(
        	{
        		success:false, 
        		message: error.message 
        	}
        );
    }
}