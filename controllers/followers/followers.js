import mongoose from 'mongoose';
import ShopModel from '../../models/shops.js';
import UserModel from '../../models/users.js';
import PostModel from '../../models/posts.js';
import ProductModel from '../../models/products.js';
const ObjectId = mongoose.Types.ObjectId;

export const createFollowers = async (req, res) => {
    try {

        if( !req.body.status ){
            return res.status(403).json(
                {
                    success:false, 
                    message:'Please enter status 1 for user follow and 2 for shop follow'
                }
            );
        }

        const followingId = req.body.followingId;
        const followerId = req.body.followerId;
        const shopId = req.body.shopId;

        let followData = {
            followingId: followingId,
            followerId: followerId,
            shopId: shopId
        }
        if( req.body.status == 1 && followingId && followerId ){
            let userObj1 = {
                userId:followingId,
                createdAt: new Date()
            };
            let userObj2 = {
                userId: followerId,
                createdAt: new Date()
            };

            const addFollower = await UserModel.findOneAndUpdate(
                {_id: ObjectId(followerId)},
                { $push: { followers: { $each: [userObj1], $sort: { createdAt: -1 }, $position: 0 } }, $inc: { totalFollowers: 1 } },
            );

            const addFollowing = await UserModel.findOneAndUpdate(
                {_id: ObjectId(followingId)},
                { $push: { following: { $each: [userObj2], $sort: { createdAt: -1 }, $position: 0 } }, $inc: { totalFollowing: 1 } }
            );
            followData.status = req.body.status
            followData.action = 'followed'
        }

        if( req.body.status == 2 && followingId && shopId ){

            let userObj = {
                userId:followingId,
                createdAt: new Date()
            };
            let shopObj = {
                shopId: shopId,
                createdAt: new Date()
            };

            const addFollower = await ShopModel.findOneAndUpdate(
                {_id: ObjectId(shopId)},
                { $push: { followers: { $each: [userObj], $sort: { createdAt: -1 }, $position: 0 } }, $inc: { totalFollowers: 1 } },
            );

            const addFollowing = await UserModel.findOneAndUpdate(
                {_id: ObjectId(followingId)},
                { $push: { following: { $each: [shopObj], $sort: { createdAt: -1 }, $position: 0 } }, $inc: { totalFollowing: 1 } }
            );
            followData.status = req.body.status
            followData.action = 'followed'
        }
        
        return res.status(200).json(
            {
                success:true,
                data: followData,
                message:'Followers successfully added'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const removeFollowers = async (req, res) => {
    try {

        const status = req.body.status;
        const userId = req.body.userId;
        const unFollowId = req.body.unFollowId;
        const shopId = req.body.shopId;

        let followData = {
            userId: userId,
            unFollowId: unFollowId,
            shopId: shopId
        }

        if( !status){
            return res.status(403).json(
                {
                    success:false, 
                    message:'Please enter status 1 for user follow and 2 for shop follow'
                }
            );
        }


        if(status == 1 && userId && unFollowId){

            const deleteFromFollowing = await UserModel.findOneAndUpdate(
                {_id: userId },
                { $pull: { following: { userId: unFollowId } }, $inc: { totalFollowing: -1 } }
            );

            const deleteFromFollower = await UserModel.findOneAndUpdate(
                { _id: unFollowId },
                { $pull: { followers: { userId: userId } }, $inc: { totalFollowers: -1 }  }  
            );
            followData.status = status
            followData.action = 'unFollowed'
        }

        if(status == 2 && userId && shopId){

            const deleteFromFollowing = await UserModel.findOneAndUpdate(
                {_id: userId },
                { $pull: { following: { shopId: shopId } }, $inc: { totalFollowing: -1 } }
            );

            const deleteFromFollower = await ShopModel.findOneAndUpdate(
                { _id: shopId },
                { $pull: { followers: { userId: userId } }, $inc: { totalFollowers: -1 }  }  
            );
            followData.status = status
            followData.action = 'unFollowed'
        }
        
        return res.status(200).json(
            {
                success:true,
                data: followData,
                message:'Followers successfully removed'
            }
        );

    } catch (error) {

        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}



export const getFollowingList = async (req, res) => {

    try {

        const skip = parseInt(req.query.skip, 10) || 0;
        const limit = parseInt(req.query.limit, 10) || 10;

        let users = await UserModel.findOne(
            { _id: req.query.userId },
            {
                fullName: 1,
                totalFollowing: 1,
                createdAt: 1,
                following: 1,
                following: { $slice:[skip, limit] }
            }
        ).populate(
            [
                {
                    path: 'following.userId',
                    select: 'fullName profileImage totalFollowers'
                },
                {
                    path: 'following.shopId',
                    select: 'shopName totalFollowers profileImage'
                }
            ]

        )
        .sort({"following.createdAt": -1});
       
        users = JSON.parse(JSON.stringify(users));

        for( let i = 0; i < users.following.length; i++){

            if(users.following[i].hasOwnProperty('userId')){

                let posts = await PostModel.find(
                    { userId: users.following[i].userId._id, images: { $exists: true, $ne: [] } },
                    { images: 1 }
                ).limit(3);

                posts = JSON.parse(JSON.stringify(posts));

                users.following[i].posts = posts;        
            }

            if(users.following[i].hasOwnProperty('shopId')){

               let products = await ProductModel.find(
                    { shopId: users.following[i].shopId._id },
                    { mainImage: 1, slug: 1 }
                ).limit(3);

               products = JSON.parse(JSON.stringify(products));
               users.following[i].products = products;
            }
        }

        return res.status(200).json(
            {
                success:true, 
                data:users,
                message:'success'
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}
export const getFollowerList = async (req, res) => {
    try {
        const users = await UserModel.findById( req.body.userId ).populate('followers.userId');
        res.status(200).json({success:true, data:users, message:'success'});
    } catch (error) {
        res.status(400).json({success:false, message: error.message });
    }
}

export const createShopFollower = async (req, res) => {
    try {
        const userId = req.body.userId;
        const shopId = req.body.shopId;
        const addFollower = await ShopModel.findOneAndUpdate(
            {_id: shopId},
            { $push: { followers: { userId: userId }  } }
        );
        const addFollowing = await UserModel.findOneAndUpdate(
            {_id: req.body.userId},
            { $push: { shopFollowing: { shopId: shopId }  } }
        );
        return res.status(200).json({ success:true, message:'Followers successfully added'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }  //no need
}

export const removeShopFollower = async (req, res) => {
    try {
        const userId = req.body.userId;
        const shopId = req.body.shopId;
        const removeFollower = await ShopModel.findOneAndUpdate(
            {_id: shopId},
            { $pull: { followers: { userId: userId }  } }
        );
        const addFollowing = await UserModel.findOneAndUpdate(
            {_id: req.body.userId},
            { $pull: { shopFollowing: { shopId: shopId }  } }
        );
        return res.status(200).json({ success:true, message:'Followers successfully removed'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    } // no need
}