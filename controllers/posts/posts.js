import PostModel from '../../models/posts.js';
import UserModel from '../../models/users.js';
import mongoose from 'mongoose';


export const getPosts = async (req, res) => {
    const { page, perPage } = req.query;
    const options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(perPage, 10) || 10,
    }
    try {
        const posts = await PostModel.paginate( {}, options);
        res.status(200).json({ success:true, data:posts, message:'success'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
} 
  
export const createPost = async (req, res) => {
    let post = req.body;
    const ObjectId = mongoose.Types.ObjectId();
    post._id = ObjectId;

    const newPost = new PostModel(post);
    try {
        await newPost.save();


        const createdPost  =  await PostModel.findOne(
            { _id: ObjectId }
        ).populate(
            [
                {
                    path: "userId",
                    select:"totalLikedPost fullName profileImage"
                },
                {
                    path: "tags.productId",
                    select:"userId shopId title slug mainImage variations"
                },
                {
                    path: "comments",
                    options: {
                        //limit: 1,
                        //sort: { created: -1},
                        //skip: req.params.pageIndex*2
                    },

                    populate : (
                        [
                            {
                                path : 'user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.mention',
                                select: 'fullName slug profileImage',
                            },

                        ]
                    ) 
                }

            ]
        );
        return res.status(201).json(
            {
                success: true, 
                data: createdPost, 
                message:'Post successfully saved!'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message
            }
        );
    }
}

export const getPostById = async (req, res) => {
    try {
        const post = await PostModel.findById(req.params.postId);
        return res.status(200).json(
            {
                success:true, 
                data:post, 
                message:'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getPostByUserId = async (req, res) => {
    try {
        const skip = parseInt(req.query.skip, 10) || 0;
        const limit = parseInt(req.query.limit, 10) || 10;

        // const post = await PostModel.find(
        //     { userId: req.params.userId },
        //     { 
        //         _id: 1, 
        //         createdAt: 1, 
        //         totalLike: 1, 
        //         totalComment: 1, 
        //         content: 1, 
        //         images: 1
        //     }
        // ) 
        // .skip(skip)
        // .limit(limit);

        const post  =  await PostModel.find(
            { userId: req.params.userId }
            // { 
            //     _id: 1, 
            //     createdAt: 1, 
            //     totalLike: 1, 
            //     totalComment: 1, 
            //     content: 1, 
            //     images: 1
            // }
        ).populate(
            [
                {
                    path: "userId",
                    select:"totalLikedPost fullName profileImage"
                },
                {
                    path: "comments",
                    options: {
                        //limit: 1,
                        //sort: { created: -1},
                        //skip: req.params.pageIndex*2
                    },

                    populate : (
                        [
                            {
                                path : 'user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.mention',
                                select: 'fullName slug profileImage',
                            },

                        ]
                    ) 
                }

            ]
        ).skip(skip).limit(limit).sort({createdAt: -1});


        let totalPost = await PostModel.find(
            { userId: req.params.userId }
        )
        
        totalPost = totalPost.length
        return res.status(200).json(
            {
                success:true, 
                data:post,
                totalItem:totalPost, 
                message:'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}
 
export const getPostByShopId = async (req, res) => {

    try {
        const skip = parseInt(req.query.skip, 10) || 0;
        const limit = parseInt(req.query.limit, 10) || 10;

        // const posts = await PostModel.find(
        //     { shopId:req.params.shopId },
            
        // ).populate('userId shopId');

        const post  =  await PostModel.find(
            { userId:req.params.shopId }
            // { 
            //     _id: 1, 
            //     createdAt: 1, 
            //     totalLike: 1, 
            //     totalComment: 1, 
            //     content: 1, 
            //     images: 1
            // }
        ).populate(
            [
                {
                    path: "userId",
                    select:"totalLikedPost fullName profileImage"
                },
                {
                    path: "comments",
                    options: {
                        //limit: 1,
                        //sort: { created: -1},
                        //skip: req.params.pageIndex*2
                    },

                    populate : (
                        [
                            {
                                path : 'user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.mention',
                                select: 'fullName slug profileImage',
                            },

                        ]
                    ) 
                }

            ]
        ).skip(skip).limit(limit).sort({createdAt: -1});




        return res.status(200).json(
            {
                success:true, 
                data:post, 
                message:'success'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const updatePost = async (req, res) => {
    const post = req.body;
    try {
        const updatePost = await PostModel.updateOne(
            { _id: req.params.postId },
            { $set: post } 
        );
        res.status(200).json({ success:true, data:updatePost, message:'Post successfully updated'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const deletePost = async (req, res) => {
    try {
        const post = await PostModel.remove({ _id: req.params.postId });
        res.status(200).json({ success:true, data:post, message:'Post successfully deleted'});
    } catch (error) {
        res.status(500).json({success:false, message: error.message });
    }
}

export const createPostComment = async (req, res) => {
    console.log(req.body.postId);
    try {
        const commentData = req.body;
        const addComment = await PostModel.findOneAndUpdate(
            {_id: req.body.postId},
            { $push: { comments: commentData  }, $inc: { totalComment: 1 } }
        );
        return res.status(201).json({success: true, data: addComment, message: 'Comment successfully created'});
    } catch (error) {
        res.status(500).json({ success:false, message: error.message });
    }
}
 
export const createPostLike = async (req, res) => {
    try {

        const likeData = req.body;
        let message = '';
        let checkItem = await PostModel.findOne(
            { _id: req.body.postId, "postLikes.userId": req.body.userId}
        )
        if(!checkItem){
            const addLike = await PostModel.findOneAndUpdate(
                {_id: req.body.postId},
                { $push: { postLikes: likeData  }, $inc: { totalLike: 1 } }
            );
            const liked = await UserModel.findOneAndUpdate(
                {_id: req.body.userId},
                { $push: { likedPost: likeData  }, $inc: { totalLikedPost: 1} }
            );
            message = 'Like Added!';
        }else{

            const removeLike = await PostModel.findOneAndUpdate(
                {_id: req.body.postId},
                { $pull: { postLikes: { userId: req.body.userId}  }, $inc: { totalLike: -1 } }
            );
            const removeliked = await UserModel.findOneAndUpdate(
                {_id: req.body.userId},
                { $pull: { likedPost: { postId: req.body.postId}  }, $inc: { totalLikedPost: -1 } }
            );
            message = 'Remove Like!';
        }
        return res.status(201).json(
            {
                success: true, 
                message: message
            }
        );
    } catch (error) {
        res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getLikedPost = async (req, res) => {
    const userId = req.query.userId;
    const skip = parseInt(req.query.skip, 10) || 0;
    const limit = parseInt(req.query.limit, 10) || 10;
    try {
        let liked = await UserModel.findOne(
            {_id: userId},
            {name: 1, likedPost: 1}
        )
        .populate(
            {
                path: "likedPost.postId",
                populate : {
                    path : 'userId',
                    select: '_id fullName',
                },
                select: '_id createdAt images content',
                options: {
                    limit: limit,
                    skip: skip
                },
            }
        );
        let totalItem = 0; 
        if(liked.likedPost){
            totalItem = liked.likedPost.length;
            liked = liked.likedPost.filter(function(val) { return val.postId != null });
        }
    
        return res.status(200).json(
            {
                success: true, 
                data: liked,
                totalItem: totalItem, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}


