
import ProductModel from '../../models/products.js';
import CategoryModel from '../../models/categories.js';
import ColorModel from '../../models/colors.js';
import SizeModel from '../../models/sizes.js';

export const getProduct = async (req, res) => {
    try {

    	// PAGINATION
    	const skip = parseInt(req.body.skip, 10) || 0;
    	const limit = parseInt(req.body.limit, 10) || 10;


    	// FILTER VARIABLE
    	const category = req.body.category;
    	const subCategory = req.body.subCategory;
    	const innerCategory = req.body.innerCategory;
    	const color = req.body.color;
    	const size = req.body.size;
    	const minPrice = req.body.minPrice;
    	const maxPrice = req.body.maxPrice;
    	const all = req.body.all;


    	// QUERY VARIABLE
    	let products;
    	let totalProduct;
    	let query;

    	// GETTING COLOR SIZE FOR FRONT END FILTER
    	let hasSize = false;
    	let hasColor = [];
    	let allColors;
    	let allSizes

    	if(!category && !subCategory && !innerCategory){
    		return res.status(400).json(
	        	{ 
	        		success:false,
	        		message:'Please enter categroy or sub category or inner category'
	        	}
	        );
    	}

        if( innerCategory ){
    
            query = ProductModel.find(
                { innerCategory: innerCategory }
            );


            // ASSIGN COLOR AND SIZE
            let checkInnerCategory = await CategoryModel.findOne(
                { "subCategory.innerCategory._id": innerCategory },
                { "subCategory.innerCategory.$": 1 }
            )
            if(checkInnerCategory){

                for( let i = 0; i < checkInnerCategory.subCategory[0].innerCategory.length; i++){
                    if( checkInnerCategory.subCategory[0].innerCategory[i]._id == innerCategory ){
                        hasColor = checkInnerCategory.subCategory[0].innerCategory[i].color;
                        hasSize = checkInnerCategory.subCategory[0].innerCategory[i].sizes;
                        break;
                    }
                }
            }
        }else if( subCategory ){

            query = ProductModel.find(
                { subCategory: subCategory }
            );

            // ASSIGN COLOR AND SIZE
            let checkSubCategory = await CategoryModel.findOne(
                { "subCategory._id": subCategory },
                { "subCategory.$":1 }
            )
            if(checkSubCategory){
                hasColor = checkSubCategory.subCategory[0].color;
                hasSize = checkSubCategory.subCategory[0].sizes;
            }

        }else{
            // PRODUCT FILTER
            query = ProductModel.find(
                { category: category }
            );

            // ASSIGN COLOR AND SIZE
            let checkCategory = await CategoryModel.findOne(
                { _id: category },
                { name: 1, color: 1, sizes: 1}
            )
            if(checkCategory){
                hasColor = checkCategory.color;
                hasSize = checkCategory.sizes;
            }
        }

    	query.getFilter();

    	if( color  && color.length != 0 ){
    		query.find(
    			{ 'variations.color': { $in: color } }

    		);
    	}

    	if( size  && size.length != 0 ){
    		query.find(
    			{ "variations.size": { $in: size } }
    		)
    	}

    	if( minPrice && maxPrice){

    		query.find(
    			{ "variations.price": { $gte : minPrice, $lte : maxPrice } }
    		)
    	}

    	query.skip( skip ).limit( limit );
    	query.populate(
    		{ 
    			path: 'userId',
    			select: 'name'
    		}
    	);

    	query.populate(
    		{ 
    			path: 'shopId',
    			select: 'shopName'
    		}
    	);


    	query.select(
    		'createdAt userId shopId title slug mainImage variations'
    	)
    	
    	products = await query.exec();


    	query.skip( 0 ).limit( Number.MAX_SAFE_INTEGER );
    	query.countDocuments();
    	totalProduct = await query.exec();



    	//GET COLOR AND SIZE FOR FRONTEND FILTER

    	if( hasColor ){
    		allColors = await ColorModel.find( {} )
    	}
    	if( hasSize && hasSize.length != 0 ){
    		allSizes = await SizeModel.find(
    			{ category: { $in: hasSize } }
    		)
    	}

        return res.status(200).json(
        	{ 
        		success:true,
        		data: products,
        		totalProduct: totalProduct,
        		allColors: allColors,
        		allSizes: allSizes,
        		message:'success'
        	}
        );
    } catch (error) {
        return res.status(500).json(
        	{
        		success:false, 
        		message: error.message 
        	}
        );
    }
}

