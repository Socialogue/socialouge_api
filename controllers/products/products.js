import ProductModel from '../../models/products.js';
import PostModel from '../../models/posts.js';
import CategoryModel from '../../models/categories.js';
import SizeModel from '../../models/sizes.js';
import ColorModel from '../../models/colors.js';
import SeasonModel from '../../models/seasons.js';
import MaterialModel from '../../models/materials.js';
import CountryModel from '../../models/countries.js';
import CarrierModel from '../../models/carriers.js';
import ShopModel from '../../models/shops.js';
import helpers from '../../lib/helpers.js';

export const createProduct = async (req, res) => {
    let product = req.body;

    

    try {

        const slugItem = await helpers.makeSlug(product.title, 'products');
        product.slug = slugItem.slug;
        product.slugId = slugItem.slugId;

        const newProduct = new ProductModel(product);
        await newProduct.save();

        return res.status(201).json(
            { 
                success:true, 
                data:newProduct, 
                message:'Product successfully created'
            }
        );

    } catch (error) {
        
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
} 

export const getAllItems = async (req, res) => {
    try {
        const sizes = await SizeModel.find({});
        const colors = await ColorModel.find({});
        const seasons = await SeasonModel.find({});
        const materials = await MaterialModel.find({});
        const categories = await CategoryModel.find({});
        const countries = await CountryModel.find({});
        const carriers = await CarrierModel.find({});
        const shippingMethod = await ShopModel.find(
            { _id: req.params.shopId },
            { shipToTemplate: 1, shipFromTemplate:1 }
        ) 

        res.status(200).json(
            {
                success:true, 
                sizes:sizes,
                colors:colors,
                seasons:seasons,
                materials:materials,
                categories:categories,
                countries:countries,
                carriers:carriers,
                shippingMethod: shippingMethod,
                message:'success'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
} 

export const getProductByUserId = async (req, res) => {
    console.log(req.body.userId);
    try {
        const product = await ProductModel.find(
            { userId:req.body.userId }
        ).populate('brands.brand category seasons.season materials.material');
        res.status(200).json({success:true, data:product, message:'success'});
    } catch (error) {
        res.status(400).json({success:false, message: error.message });
    }
}
export const getProductById = async (req, res) => {

    try {
        const product = await ProductModel.findById(
            req.params.productId 
        ).populate('userId shopId');
        res.status(200).json(
            {
                success:true, 
                data:product, 
                message:'success'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const getProductByShopId = async (req, res) => {

    try {
        const product = await ProductModel.find(
            { shopId:req.params.shopId },
            { shippingFrom: 0, materials: 0, seasons: 0, tags: 0, shippingTo: 0 }
        ).populate('userId shopId');
        return res.status(200).json(
            {
                success:true, 
                data:product, 
                message:'success'
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}


export const searchProduct = async (req, res) => {

    try {

        const userId = req.body.userId;
        const status = req.body.status; // 1 = my shop product 2 = socilauge shop all shop
        const search = req.body.search; // 4 = my shop all items

        let products;

        if( status == 1 ){
            products = await ProductModel.find(
                { 
                    userId: userId, 
                    title: { $regex: '.*' + search + '.*', $options: 'i' }
                },
                { userId: 1, shopId: 1, title: 1, slug: 1, mainImage: 1, variations: 1}
            ).sort({createdAt: -1}).limit(20);

        }else if( status == 2){

            products = await ProductModel.find(
                { 
                    title: { $regex: '.*' + search + '.*', $options: 'i' }
                },
                { userId: 1, shopId: 1, title: 1, slug: 1, mainImage: 1, variations: 1}
            ).sort({createdAt: -1}).limit(20);
        }else if( status == 4 ){

            products = await ProductModel.find(
                { 
                    userId: userId 
                },
                { userId: 1, shopId: 1, title: 1, slug: 1, mainImage: 1, variations: 1}
            ).sort({createdAt: -1});
        }
        
        return res.status(200).json(
            {
                success:true, 
                data: products,
                message:'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}





export const updateProduct = async (req, res) => {
    const product = req.body;
    try {
        const updateProduct = await ProductModel.updateOne(
            { _id: req.params.productId },
            { $set: product } 
        );
        res.status(200).json({ success:true, data:updateProduct, message:'Product successfully updated'});
    } catch (error) {
        res.status(400).json({success:false, message: error.message });
    }
}

export const deleteProduct = async (req, res) => {
    try {
        const product = await ProductModel.deleteOne({ _id: req.params.productId });
        res.status(200).json({ success:true, data:post, message:'Product successfully deleted'});
    } catch (error) {
        res.status(400).json({success:false, message: error.message });
    }
}


