import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import hashPassword from '../../lib/hashPassword.js';
import UserModel from '../../models/users.js';
import { sendConfirmationMail, saveOtp } from '../auth/register.js';
import VerificationModel from '../../models/verifications.js';
import ShopModel from '../../models/shops.js';
const ObjectId = mongoose.Types.ObjectId;
import helpers from '../../lib/helpers.js';

export const getProfile = async (req, res) => {
    try {
        const users = await UserModel.aggregate([
            { 
                $match: {
                    _id: ObjectId(req.body.userId) 
                } 
            },
            {
                $lookup: {
                    from: "shops",
                    localField: "_id",
                    foreignField: "userId",
                    as: "shops"
                }
            }
 
        ])
        res.status(200).json({ success:true, data:users, message:'success'});
    } catch (error) {
        res.status(400).json({success:false, message: error.message });
    }
}

export const getCurrentProfileInfo = async ( req, res) => {
    try {
        const profile =  await UserModel.findById(
            { _id: req.body.authUserId },
            {
                nickName: 1,
                gender: 1,
                name: 1,
                fullName: 1,
                country: 1,
                phone: 1,
                email: 1,
                lastPasswordChange: 1
            }
        )
        return res.status(200).json(
            { 
                success:true, 
                data: profile,
                message:'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}

export const updateProfile = async (req, res) => {
    const profile = req.body;
    try {

        const slugItem = await helpers.makeSlug(profile.fullName, 'users');
        profile.slug = slugItem.slug;
        profile.slugId = slugItem.slugId;


        const updateProfile = await UserModel.updateOne(
            { _id: req.body.authUserId },
            { $set: profile } 
        );
        return res.status(200).json({ success:true, message:'Profile successfully updated', data:updateProfile});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}

export const getDeliveryInfo = async (req, res) => {
    const deliveryInfo = req.body;
    try {
        const getDeleveryInfo = await UserModel.find(
            { _id: req.body.authUserId },
            { delivery: 1}
        )
        
        return res.status(200).json(
            { 
                success:true, 
                data:getDeleveryInfo,
                message:'success', 
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const addDeliveryInfo = async (req, res) => {
    const deliveryInfo = req.body;
    try {
        if(req.body.billing){
            await UserModel.updateMany(
                { _id: req.body.authUserId },
                { $set: { "delivery.$[elem].billing" : 0 } },
                { arrayFilters: [ { "elem.billing": { $gte: 0 } } ] }
            );
            deliveryInfo.billing = 1;
        }
        deliveryInfo.billing = 0;

        const updateProfile = await UserModel.updateOne(
            { _id: req.body.authUserId },
            { $push: { delivery:deliveryInfo } } 
        );

        const getDeleveryInfo = await UserModel.find(
            { _id: req.body.authUserId },
            { delivery: 1}
        )
        
        return res.status(200).json(
            { 
                success:true,
                data: getDeleveryInfo,
                message:'Delivery information successfully saved', 
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const deleteDeliveryInfo = async (req, res) => {
    console.log(req.params.id);
    try {
        const deleteDinfo = await UserModel.findOneAndUpdate(
            { _id: req.body.authUserId },
            { $pull: { delivery: { _id: req.params.id }  } } 
        );
        const getDeleveryInfo = await UserModel.find(
            { _id: req.body.authUserId },
            { delivery: 1}
        )
        
        return res.status(200).json(
            { 
                success:true,
                data: getDeleveryInfo,
                message:'Delivery information successfully deleted', 
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}



export const updateDeliveryInfo = async (req, res) => {
    const deliveryInfo = req.body;
    try {
        const updateProfile = await UserModel.findOneAndUpdate(
            { "delivery._id": req.body.id },
            { $set: { "delivery.$":deliveryInfo } } 
        );
        const getDeleveryInfo = await UserModel.find(
            { _id: req.body.authUserId },
            { delivery: 1}
        )
        if(deliveryInfo.billing){
            deliveryInfo.billing = 1;
        }
        deliveryInfo.billing = 0;
        return res.status(200).json(
            { 
                success:true,
                data: getDeleveryInfo,
                message:'Delivery information successfully udated', 
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}



export const getProfileByArray = async (req, res) => {
    const list = req.body.userList;
    try {
        const users = await UserModel.find(
            { 
                _id: { $in:list }
            }
        );
        return res.status(200).json({ success:true, data:users, message:'success'});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }   

}

export const sendOtpForChangeEmail = async (req, res) => {
    try {
        const currentUser = await UserModel.findById( req.body.authUserId );
        if( req.body.email != currentUser.email ){
            return res.status(400).json(
                { 
                    success:false, 
                    message:'Your Current email does not match our record!'
                }
            );
        }
        const existEmail = await UserModel.findOne(
            { email: req.body.newEmail}
        )
        if(existEmail){
            return res.status(409).json(
                { 
                    success:false, 
                    message:'New Email Already Exist!'
                }
            );
        }

        const otp = await saveOtp(currentUser.email );
        const mailObj = {
            email: req.body.newEmail
        }
        const sendMail = await sendConfirmationMail(mailObj, otp);
        const data = {
            username: currentUser.email,
            newEmail: req.body.newEmail
        }
        
        return res.status(200).json(
            { 
                success:true,
                data: data, 
                message:'Otp has been sent to your new email. please verify!'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }   

}
export const changeEmail = async (req, res) => {
    try {
        const user = await VerificationModel.findOne(
            { username: req.body.username }
        );
        if(!user){
            return res.status(400).json(
                {
                    success:false, 
                    message: 'Invalid OTP' 
                }
            );
        }
        
        if(user.otp == req.body.otp){
            const deleteOtp = await VerificationModel.deleteOne(
                { username:req.body.username}
            );
            const updateNewEmail = await UserModel.updateOne(
                { _id: req.body.authUserId },
                { $set: { email: req.body.newEmail } } 
            );
            return res.status(200).json(
                {
                    success: true, 
                    message:'Email has been successfully changed'
                }
            );
        }else{
            return res.status(400).json(
                {
                    success: false, 
                    message: 'Invalid OTP' 
                }
            );
        }
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error
            }
        );
    }
}

export const changePassword = async (req, res) => {
    try {
        if(req.body.newPassword != req.body.confirmPassword){
            return res.status(400).json(
                { 
                    success:false, 
                    message:'New password not match with confirm password'
                }
            );
        }

        const user = await UserModel.findById( req.body.authUserId );
        const match = await bcrypt.compare(req.body.currentPassword, user.password);
        if(match){
            const passwodHassed = await hashPassword.getHashedPassword(req.body.newPassword);
            const updateNewPassword = await UserModel.updateOne(
                { _id: req.body.authUserId },
                { $set: { password: passwodHassed, lastPasswordChange: new Date() } } 
            );
            return res.status(200).json(
                { 
                    success:true, 
                    message:'Password Has been change'
                }
            );
        }
        return res.status(401).json({
            success: false,
            message: 'Current password does not match our record!'
        });

          
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }   

}
export const deleteAccount = async (req, res) => {
    try {
        const delOjb = {
            status: 1,
            date: new Date()
        }
        const softDelete = await UserModel.updateOne(
            { _id: req.body.authUserId },
            { $set: { softDelete: delOjb } } 
        );
        return res.status(200).json(
            { 
                success:true, 
                message:'Account Has been deleted'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}


export const imageUpload = async (req, res) => {
    try {
        const image = req.body.image;
        
        const type = req.body.type;

        const status = req.body.status;

        const shopId = req.body.shopId;

        if( status == 'profile'){

            const addImage = await UserModel.update(
                { _id: req.body.userId },
                { $set: { [type]: image } }
            );
        }

        if( status == 'shop'){

            const addImage = await ShopModel.update(
                { _id: shopId },
                { $set: { [type]: image } }
            );
        }

        return res.status(200).json(
            { 
                success:true, 
                message:'Upload Success'
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}