import PostModel from '../../models/posts.js';
import UserModel from '../../models/users.js';



export const getUser = async (req, res) => {
    try {
        const user = await UserModel.findOne(
            { _id: req.params.userId},
            { _id: 1, fullName: 1, profileImage: 1, totalFollowers: 1}
        )
        return res.status(200).json(
            { 
                success: true, 
                data: user, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
    
}

export const getPosts = async (req, res) => {
    try {
        const skip = parseInt(req.query.skip, 10) || 0;
        const limit = parseInt(req.query.limit, 10) || 10;

        // const posts = await PostModel.find(
        //     { userId: req.params.userId},
        //     { createdAt: 1, totalLike: 1, totalComment: 1, content: 1, images: 1}
        // ).skip(skip).limit(limit);

        const post  =  await PostModel.find(
            { userId: req.params.userId }
            // { 
            //     _id: 1, 
            //     createdAt: 1, 
            //     totalLike: 1, 
            //     totalComment: 1, 
            //     content: 1, 
            //     images: 1
            // }
        ).populate(
            [
                {
                    path: "userId",
                    select:"totalLikedPost fullName profileImage"
                },
                {
                    path: "comments",
                    options: {
                        //limit: 1,
                        //sort: { created: -1},
                        //skip: req.params.pageIndex*2
                    },

                    populate : (
                        [
                            {
                                path : 'user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.user',
                                select: 'fullName slug profileImage',
                            },

                            {
                                path : 'replay.mention',
                                select: 'fullName slug profileImage',
                            },

                        ]
                    ) 
                }

            ]
        ).skip(skip).limit(limit).sort({createdAt: -1});

        

        let totalPost = await PostModel.find(
            { userId: req.params.userId }
        )
        
        totalPost = totalPost.length
        return res.status(200).json(
            { 
                success: true, 
                data: post,
                totalItem:totalPost,
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
    
}
export const getFavProduct = async (req, res) => {

    try {
        const skip = parseInt(req.query.skip, 10) || 0;
        const limit = parseInt(req.query.limit, 10) || 10;

        const products = await UserModel.findOne(
            { _id: req.params.userId },
            { 
                _id: 1, 
                name: 1, 
                TotalfavoriteProduct:1,
                favoriteProduct: { $slice:[skip, limit] } 
            }
        ).populate(
            {
                path: 'favoriteProduct.productId',
                populate:[
                    { path: 'userId', select: 'fullName' },
                    { path: 'shopId', select: 'shopName _id' }
                ],
                select: 'title shopId price name mainImage'
            }
        );
        
        let totalProduct = 0
        if(products){
            totalProduct = products.TotalfavoriteProduct
        }
        return res.status(200).json(
            { 
                success: true, 
                data: products, 
                totalItem: totalProduct,
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
     
}
export const getFollowing = async (req, res) => {
    try {
        const products = await UserModel.find(
            { _id: req.params.userId},
            { "_id": 1, "name": 1 }
        ).populate('following.userId','name totalFollowers profileImage')
        return res.status(200).json(
            { 
                success: true, 
                data: products, 
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
    
}