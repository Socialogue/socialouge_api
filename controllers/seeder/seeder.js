import faker from "faker";
import ShopModel from '../../models/shops.js';
import UserModel from '../../models/users.js';
import CategoryModel from '../../models/categories.js';
import CountryModel from '../../models/countries.js';
import CarrierModel from '../../models/carriers.js';
import PostModel from '../../models/posts.js';
import ProductModel from '../../models/products.js';
import helpers from '../../lib/helpers.js';
 
export const createUser = async (req, res) => {
    try {
        let dt = [];

        for (var i = 1; i <= 10; i++) {
            let fullName = faker.name.findName();
            const slugItem = await helpers.makeSlug(fullName, 'users');
            const userData = {
                fullName: fullName,
                slug: slugItem.slug,
                slugId: slugItem.slugId,
                nickName: faker.name.firstName(),
                username: `test${i}@email.com`,
                email: `test${i}@email.com`,
                password:
                "$2b$12$ixDhJistk89lzbDimdbyguPAelwH5AQ6MEExdYbAzPYzF3zeErY3m", //123456
                profileImage: "1623585854_fkKP0rG8TByx7vpHSxji.jpg",
                coverImage: "1623585854_fkKP0rG8TByx7vpHSxji.jpg",
                accountType: 1,
                verified: true,
            };

            dt.push(userData);
        }
        
        const saveUser = UserModel.insertMany(dt);

        return res.status(201).json(
            {
                success: true,
                message: 'User created successfully'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success: false, 
                message: error.message 
            }
        );
    }
}

export const createShop = async (req, res) => {
    try {
        let dataSet = [];
        let users = await UserModel.find({});
        for( var i = 0; i < users.length; i++){
            for(var j = 1; j < 3; j++){
                let shopName = "Diamond Shop "+j;
                const slugItem = await helpers.makeSlug(shopName, 'shops');
                let shopObj = {
                    userId: users[i]._id,
                    shopName: shopName,
                    slug: slugItem.slug,
                    slugId: slugItem.slugId,
                    companyId:"4343434",
                    country:"Lithunia",
                    companyAddress:"Lithunia",
                    zipCode:"1209",
                    companyPhone:"+98797879",
                    supportPhone:"+9878979",
                    email:"test@gmail.com",
                    supportEmail:"test@gmail.com"
                }
                dataSet.push(shopObj);
            }
        }
        await ShopModel.insertMany(dataSet);
        return res.status(201).json(
            {
                success: true,
                data:dataSet, 
                message: 'Shop created successfully'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success: false, 
                message: error.message 
            }
        );
    }
}
export const createCategory = async (req, res) => {
    try {
        const dataSet = [];
        for(var i = 1; i < 3; i++){
            let catName = 'WOMEN';
            if(i == 2){
                catName = 'MAN';
            }
            const catSlug = await helpers.makeSlug(catName, 'categories');
            var categoryData = {
                name:catName,
                slug: catSlug.slug,
                slugId: catSlug.slugId,
                color: true,
                sizes:["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],
                subCategory:[]

            }
            for( var j = 1; j < 6; j++){
                let subCatname = 'Sub Category '+i+'.'+j;
                const subCatSlug = await helpers.makeSlug(subCatname, 'categories');
                var subCategoryData = {
                    name:subCatname,
                    slug:subCatSlug.slug,
                    slugId:subCatSlug.slugId,
                    color: true,
                    sizes:["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],
                    innerCategory:[]
                }
                for( var k = 1; k < 6; k++){
                    let innerCatName = 'Inner Category '+i+'.'+j+'.'+k;
                    const innerCatSlug = await helpers.makeSlug(innerCatName, 'categories');
                    var innerCategoryData = {
                        name:innerCatName,
                        slug: innerCatSlug.slug,
                        slugId: innerCatSlug.slugId,
                        color: true,
                        sizes:["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],
                    }
                    subCategoryData.innerCategory.push(innerCategoryData);
                }

                categoryData.subCategory.push(subCategoryData);
            }
            
            dataSet.push(categoryData);
        }
        const saveData = await CategoryModel.insertMany(dataSet);
        return res.status(200).json(
            {
                success: true, 
                data:dataSet,
                message: 'success'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
    
}
export const createCarrier = async (req, res) => {
    try {
        let countries = await CountryModel.find({});
        let carrires = ["USPS","MSC","CMA-CGM","Hapag-Lloyd","ONE"];
        let DatObje = [];
        for(var i = 0; i < countries.length; i++){
            for(var j = 0; j < 5; j++){
                const carrierData = {
                    name: carrires[j],
                    countryId: countries[i]._id
                };
                DatObje.push(carrierData);
            }
        }
        const newCarrier =  CarrierModel.insertMany(DatObje);
        res.status(200).json(
            { 
                success:true, 
                data:DatObje, 
                message:"Carrier seed successfully", 
            }
        );
    } catch (error) {
        res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}
export const createPost = async (req, res) => {
    try {
        let dataSet = [];
        let imgList = [
            "1623586393_ViJasaORghKMtmMVguBI.jpg",
            "1623586434_KpilU2I3eSzbEAUqEjqU.jpg",
            "1623586468_j3JyKSMtlJbV3SQ4wbbk.jpg",
            "1624871086_Q8vinQwiTvfAWaGi5tpv.jpg"
        ];
        let videoList = [
            "https://youtu.be/EngW7tLk6R8",
            "https://youtu.be/O3QvgVLMmUY",
            "https://youtu.be/r9dd6csVZbk",
            "https://youtu.be/aNVviTECNM0"
        ]
        let users = await UserModel.find({});
        for( var i = 0; i < users.length; i++){
            for(var j = 1; j < 11; j++){
                let postObj = {
                    userId: users[i]._id,
                    content:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
                    images:[],
                    videos:[],
                    comments:[],
                    totalComment: 0,
                    postLikes:[],
                    totalLike: 0,
                    tags:[]
                }
                for( var k = 0; k < 4; k++){
                    let imgObj = {
                        url:imgList[k]
                    }
                    let videoObj = {
                        url:videoList[k]
                    }
                    postObj.images.push(imgObj);
                    postObj.videos.push(videoObj);
                }
                dataSet.push(postObj);
            }
        }
        await PostModel.insertMany(dataSet);
        return res.status(201).json(
            {
                success: true,
                data:dataSet, 
                message: 'Post created successfully'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success: false, 
                message: error.message 
            }
        );
    }
}
export const createProduct = async (req, res) => {
    try {
        let dataSet = [];
        let imgList = [
            "1623586393_ViJasaORghKMtmMVguBI.jpg",
            "1623586434_KpilU2I3eSzbEAUqEjqU.jpg",
            "1623586468_j3JyKSMtlJbV3SQ4wbbk.jpg",
            "1623586393_ViJasaORghKMtmMVguBI.jpg"
        ];
        let oneOrZero = (Math.random()>=0.5)? 1 : 0;
        let shops = await ShopModel.find({});
        let categories = await CategoryModel.find({});
        for( var i = 0; i < shops.length; i++){
            for(var j = 0; j < 10; j++){
                let productTitle = "Long Soft Cotton Dress with Pockets "+i+' '+j;
                const slugItem = await helpers.makeSlug(productTitle, 'products');
                let productObj = {
                    userId: shops[i].userId,
                    shopId: shops[i]._id,
                    category: categories[oneOrZero]._id,
                    title: productTitle,
                    slug: slugItem.slug,
                    slugId: slugItem.slugId,
                    mainImage:"https://i.pinimg.com/736x/1d/b1/cb/1db1cb6398f29f5775c4c1ca05a4deed.jpg",
                    additionalImage:[],
                    variations:[],
                    shippingFrom:null,
                    shippingTo: [],
                    description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
                    materials:["Granite","Frozen"],
                    seasons:["Summer","Winter"],
                    tags:[],
                    followers:[],
                    following:[]
                }
                // add aditional images
                for( var k = 0; k < 4; k++){
                    productObj.additionalImage.push(imgList[k]);
                }
                // add variations
                for( var vari = 0; vari < 4; vari++){
                    let variationsObj = {
                        sizes:{lt:20,uk:38,us:49, eu:47},
                        color: { name: faker.commerce.color()},
                        colorImage:imgList[vari],
                        sku: "ABC-4",
                        quantity: (+vari + 1) * 5,
                        price: (+vari + 1) * 10
                    }
                    productObj.variations.push(variationsObj);
                }
                // add shipping form
                // let shippingFormObj = {
                //     country: faker.address.country(),
                //     processingTime: 2,
                //     carrier: "USPS",
                //     deliveryTime:{
                //         from: 2,
                //         to: 5
                //     },
                //     shippingPrice: 10,
                //     oneItemPrice: 5,
                //     additionalItemPrice: 7
                // }
                // productObj.shippingFrom = shippingFormObj;

                // // add shipping to
                // for( var shipTo = 0; shipTo < 4; shipTo++){
                //     let shippingToObj = {
                //         country: faker.address.country(),
                //         oneItemPrice: (+shipTo + 1) * 5,
                //         additionalItemPrice: (+shipTo + 1) * 7
                //     }
                //     productObj.shippingTo.push(shippingToObj);
                // }
                dataSet.push(productObj);
            }
        }
        await ProductModel.insertMany(dataSet);
        return res.status(201).json(
            {
                success: true,
                data:dataSet, 
                message: 'Product created successfully'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success: false, 
                message: error.message 
            }
        );
    }
}