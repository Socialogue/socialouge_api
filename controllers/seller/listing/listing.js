
import ProductModel from '../../../models/products.js';


export const getListing = async (req, res) => {

    try {
        const { page, limit } = req.query;
        const options = {
            page: parseInt(page, 10) || 1,
            limit: parseInt(limit, 10) || 10,
            select: 'userId shopId mainImage title slug  variations',
        }

        const listing = await ProductModel.paginate( 
            { shopId: req.params.shopId},
            options
        );

        
        return res.status(200).json(
            {
                success:true, 
                data:listing, 
                message:'success'
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );

    }

}

export const deleteListing = async (req, res) => {

    try {
        

        if(Array.isArray(req.body.listingItems)){

            await ProductModel.deleteMany(
                { _id: { $in: req.body.listingItems } }
            );

        }else{

            return res.status(500).json(
                {
                    success:false, 
                    message:'Please provide a valid array of product id'
                }
            );
        }
        
        return res.status(200).json(
            {
                success:true, 
                message:'Product successfully deleted'
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
        
    }

}

export const changeQuantityOrPrice = async (req, res) => {

    try {

        if(req.body.price == '' || req.body.quantity == '' ){
            return res.status(500).json(
                {
                    success: false, 
                    message: 'please enter valid price or quantity' 
                }
            );
        }

        let flag = 'Quantity';

        let updateItem;

        if(req.body.quantity){

            updateItem = await ProductModel.updateOne(
                { _id: req.body.productId, "variations._id": req.body.variationId },
                { $set: { "variations.$.quantity": req.body.quantity } } 

            );
        }

        if(req.body.price){

            flag = 'Price';

            updateItem = await ProductModel.updateOne(
                { _id: req.body.productId, "variations._id": req.body.variationId },
                { $set: { "variations.$.price": req.body.price } } 

            );

        }
        
        return res.status(200).json(
            {
                success:true,
                message:`${flag} is updated`
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
        
    }

}
