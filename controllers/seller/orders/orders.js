
import OrderModel from '../../../models/orders.js';


export const getOrders = async (req, res) => {

    try {
        const { page, limit } = req.query;
        const options = {
            page: parseInt(page, 10) || 1,
            limit: parseInt(limit, 10) || 10,
            select: 'shop user customer productName totalCost  orderStatus model quantity orderDate productImage',
            populate: {
                path: 'user',
                select: 'name'
            }

        }

        const orders = await OrderModel.paginate( 
            { shop: req.params.shopId},
            options
        ); 

        
        return res.status(200).json(
            {
                success:true, 
                data:orders, 
                message:'success'
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );

    }

}

export const createOrder = async (req, res) => {

    try {
        const order = req.body;
        const newOrder = new OrderModel(order);
        await newOrder.save();
        
        return res.status(201).json(
            {
                success:true, 
                message:'Order placed successfully'
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );

    }

}


export const changeOrderStatus = async (req, res) => {

    try {

        const orderIds = req.body.orderIds;
        const orderStatus = req.body.orderStatus;

        if(!Array.isArray(orderIds)){
            return res.status(500).json(
                {
                    success: false, 
                    message: 'please enter array or order id' 
                }
            );
        }
        if(orderStatus == ''){
            return res.status(500).json(
                {
                    success: false, 
                    message: 'please enter order status' 
                }
            );
        }

        let updateItem;

        if(orderStatus == 'New'){
            updateItem = await OrderModel.updateMany(
                { _id: { $in: orderIds } },
                { $set: { "orderStatus": orderStatus, "shippedDate": '' } } 
            )
        }

        if(orderStatus == 'Shipped'){

            updateItem = await OrderModel.updateMany(
                { _id: { $in: orderIds } },
                { $set: { "orderStatus": orderStatus, "shippedDate": new Date() } } 
            )
        }
        
        return res.status(200).json(
            {
                success:true,
                data: updateItem,
                message: 'Order status has been changed'
            }
        );

    } catch (error) {

        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
        
    }

}
