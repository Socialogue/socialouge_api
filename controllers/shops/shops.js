import ProductModel from '../../models/products.js';
import ShopModel from '../../models/shops.js';
import helpers from '../../lib/helpers.js';

export const createShop = async (req, res) => {
    let shop = req.body;
    
    try {

        const slugItem = await helpers.makeSlug(shop.shopName, 'shops');
        shop.slug = slugItem.slug;
        shop.slugId = slugItem.slugId;

        const newShop = new ShopModel(shop);
        await newShop.save();
        const shops = await ShopModel.find(
            { userId: req.body.userId},
            {followers: 0, shipFromTemplate:0, shipToTemplate:0}
        );

        return res.status(201).json(
            {
                success: true, 
                message: 'Shop created successfully', 
                data:shops
            }
        ); 
    } catch (error) {
        return res.status(500).json(
            {
                success: false, 
                message: error.message 
            }
        );
    }
}

export const getShopByUserId = async (req, res) => {
    try {
        const shops = await ShopModel.find(
            { userId: req.body.authUserId},
            {followers: 0, shipFromTemplate:0, shipToTemplate:0}
        );
        return res.status(200).json({ success:true, message:'success', data:shops});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}

export const getAllShop = async (req, res) => {
    try { 
        const skip = parseInt(req.query.skip, 10) || 0;
        const limit = parseInt(req.query.limit, 10) || 10;

        //console.log(skip,limit);

        const shops = await ShopModel.aggregate([
            { $skip: skip },
            { $limit: limit},
            {
                $lookup: {
                    from: "products",
                    as: "products",
                    let: { shopId: '$_id' },
                    pipeline: [
                        { $limit: 3 }
                    ]
                }
            },
            { 
                $project: {
                    shopName: 1,
                    userId: 1,
                    profileImage: 1,
                    coverImage: 1, 
                    totalFollowers: 1,
                    "products._id": 1,
                    "products._id": 1, 
                    "products.title": 1,
                    "products.mainImage": 1,
                    "products.variations": 1
                } 
            }

        ])

        let totalShop = await ShopModel.count({});

        return res.status(200).json(
            { 
                success:true, 
                message:'success', 
                data:shops,
                totalItem:totalShop
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success:false, 
                message: error.message 
            }
        );
    }
}


export const getShopById = async (req, res) => {
    try {
        const shop = await ShopModel.findById(req.params.shopId);
        return res.status(200).json({ success:true, message:'success', data:shop});
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message });
    }
}

export const updateShop = async (req, res) => {
    const shop = req.body;
    try {

        const slugItem = await helpers.makeSlug(shop.shopName, 'shops');
        shop.slug = slugItem.slug;
        shop.slugId = slugItem.slugId;
        
        const updateShop = await ShopModel.updateOne(
            { _id: req.params.shopId },
            { $set: shop } 
        );
        const activeShop = await ShopModel.findOne(
            { _id: req.params.shopId },
            { followers: 0, shipFromTemplate:0, shipToTemplate:0 }
        );
        console.log(activeShop);
        return res.status(200).json(
            { 
                success:true, 
                message:'Shop successfully updated', 
                data:activeShop
            }
        );
    } catch (error) {
        return res.status(500).json(
            { 
                success:false, 
                message: error.message 
            }
        );
    }
}

export const deleteShop = async (req, res) => {
    try {
        const shop = await ShopModel.deleteOne({ _id: req.params.shopId });
        return res.status(200).json({ success:true, data: shop, message: 'Shop Successfully Deleted' });
    } catch (error) {
        return res.status(500).json({ success:false, message: error.message});
    }
}

export const getProductByShop = async (req, res) => {
    try {
        const products = await ProductModel.find({ shopId: req.body.shopId}).populate("userId brands.brand category seasons.season materials.material");
        return res.status(200).json({ success:true, message:'success', data:products});
    } catch (error) {
        return res.status(400).json({ success:false, message: error.message });
    }
}

export const addTempleteToShop = async (req, res) => {
    try {
        const shipName = req.body.status;
        const saveTemplete = await ShopModel.findOneAndUpdate(
            { _id: req.body.shopId },
            { $push: { [shipName]: req.body  } }
        );

        return res.status(201).json(
            {
                success: true, 
                data: saveTemplete,
                message: 'Templete Saved successfully'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success: false, 
                message: error.message 
            }
        );
    }
}

export const deleteTempleteFromShop = async (req, res) => {
    try {
        const shipName = req.params.status;
        
        const deleteTemplete = await ShopModel.findOneAndUpdate(
            { $pull: { [shipName]: { _id: req.params.tempId }  } } 
        );

        return res.status(200).json(
            {
                success: true, 
                message: 'Templete deleted successfully'
            }
        );
    } catch (error) {
        return res.status(500).json(
            {
                success: false, 
                message: error.message 
            }
        );
    }
}