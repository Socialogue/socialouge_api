import {createDummyUsers} from "./seedFiles/usersSeeder.js";
import {createDummySize} from "./seedFiles/sizeSeeder.js";
import {createDummyColor} from "./seedFiles/colorSeeder.js";
import {createDummySeason} from "./seedFiles/seasonSeeder.js";
import {createDummyMaterial} from "./seedFiles/materialSeeder.js";
import {createDummyCountries} from "./seedFiles/countrySeeder.js"

export const config = async () => {
  return [
    {
      collection: 'users',
      dataArray: await createDummyUsers()
    },
    {
      collection: 'sizes',
      dataArray: await createDummySize()
    },
    {
      collection: 'colors',
      dataArray: await createDummyColor()
    },
    {
      collection: 'seasons',
      dataArray: await createDummySeason()
    },
    {
      collection: 'materials',
      dataArray: await createDummyMaterial()
    },
    {
      collection: 'countries',
      dataArray: await createDummyCountries()
    }
  ]
  
}
