import { default as mongodb } from 'mongodb';
const MongoClient = mongodb.MongoClient;
import dotenv from 'dotenv';

import {config} from "./config.js";

dotenv.config();

const uri = process.env.CONNECTION_URL;
const client = new MongoClient(uri, {
  userNewUrlParser: true,
  useUnifiedTopology: true,
});

async function run() {

  try {

    const configArray = await config();

    configArray.map( async item => {
      await client.connect();
      const database = client.db(process.env.DB_NAME);
      const collection = database.collection(item.collection);
      await collection.insertMany(item.dataArray);
      await client.close();
    });

  } finally {
    // Ensures that the client will close when you finish/error
  }
}
run().then(() => console.log('doneee!')).catch(() => process.exit(1));
