import faker from "faker";
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

export const createDummyCategory = async () => {

    let dataSet = [
        {
            name:"Electronic Devices",
            subCategory:[
                {
                    name:"Laptop",
                    _id: ObjectId,
                    innerCategory:[
                        {
                            name:"Gamming Laptop",
                            _id: ObjectId,
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"Gamming Laptop Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"Gamming Laptop Inner 2"
                                }
                            ]
                        },
                        {
                            name:"Mack Book",
                            _id: ObjectId,
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"Mack Book Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"Mack Book Inner 2"
                                }
                            ]
                        },
                    ]
                },
                {
                    name:"Tablet",
                    _id: ObjectId,
                    innerCategory:[
                        {
                            _id: ObjectId,
                            name:"Gamming Table",
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"Gamming Table Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"Gamming Table Inner 2"
                                }
                            ]
                        },
                        {
                            name:"Mack Book Tablet",
                            _id: ObjectId,
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"Mack Book Tablet Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"Mack Book Tablet Inner 2"
                                }
                            ]
                            
                        }
                    ]
                },
                {
                    name:"Dasktop",
                    _id: ObjectId,
                    innerCategory:[
                        {
                            name:"Gamming Dasktop",
                            _id: ObjectId,
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"Gamming Dasktop Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"Gamming Dasktop Inner 2"
                                }
                            ]
                            
                        },
                        {
                            name:"All in one",
                            _id: ObjectId,
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"All in one Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"All in one Inner 2"
                                }
                            ]
                        }
                    ]
                },
                {
                    name:"Cameras",
                    _id: ObjectId,
                    innerCategory:[
                        {
                            name:"DSLR",
                            _id: ObjectId,
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"DSLR Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"DSLR Inner 2"
                                }
                            ]
                        },
                        {
                            name:"Point & Shoot",
                            _id: ObjectId,
                            innerCategory2:[
                                {
                                    _id: ObjectId,
                                    name:"Point & Shoot Inner 1"
                                },
                                {
                                    _id: ObjectId,
                                    name:"Point & Shoot Inner 2"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ];


    return dataSet;
    
};
