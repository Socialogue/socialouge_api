import faker from "faker";

export const createDummyColor = async () => {

  let dataSet = [];

  for (var i = 1; i <= 20; i++) {
    const colorData = {
      name: faker.commerce.color()
    };
    dataSet.push(colorData);
  }

  return dataSet;
};
