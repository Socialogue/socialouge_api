import faker from "faker";

export const createDummyMaterial = async () => {

  let dataSet = [];

  for (var i = 0; i <= 100; i++) {
    const materialData = {
        name: faker.commerce.productMaterial()
    };
    dataSet.push(materialData);
  }
  return dataSet;
};
 