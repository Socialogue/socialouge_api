import faker from "faker";

export const createDummySeason = async () => {

  let dataSet = [];
  let seasonDataSet = ["Summer","Winter","Spring","Fall"];

  for (var i = 0; i <= 3; i++) {
    const seasonData = {
        name: seasonDataSet[i]
    };
    dataSet.push(seasonData);
  }
  return dataSet;
};
