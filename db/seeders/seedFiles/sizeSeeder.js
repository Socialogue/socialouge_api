import faker from "faker";

export const createDummySize = async () => {

  let dataSet = [];

  for (var i = 1; i <= 200; i++) {
    const sizeData = {
      lt: faker.random.number({'min': 10,'max': 50}),
      uk: faker.random.number({'min': 10,'max': 50}),
      us: faker.random.number({'min': 10,'max': 50}),
      eu: faker.random.number({'min': 10,'max': 50}),
      category: faker.random.number({'min': 1,'max': 12})
    };
    dataSet.push(sizeData);
  }

  return dataSet;
};
