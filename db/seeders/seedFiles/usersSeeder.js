import faker from "faker";

export const createDummyUsers = async () => {

  let dt = [];

  for (var i = 1; i <= 10; i++) {
    const userData = {
      name: faker.name.findName(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: `test${i}@email.com`,
      email: `test${i}@email.com`,
      password:
        "$2b$12$ixDhJistk89lzbDimdbyguPAelwH5AQ6MEExdYbAzPYzF3zeErY3m", //123456
      profileImage: faker.image.imageUrl(),
      coverImage: faker.image.imageUrl(),
      accountType: 1,
      verified: true,
    };
      dt.push(userData);
  }

  return dt;
};
