import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';
import { createServer } from 'http';
import { Server } from 'socket.io';

// APP CONFIGURATION
const app = express();
dotenv.config();


app.use(cors());
app.options('*', cors());
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));



//ADMIN ROUTE IMPORT
import colorRoutes from './routes/admin/colors.js';
import brandRoutes from './routes/admin/brands.js';
import seasonRoutes from './routes/admin/seasons.js';
import materialRoutes from './routes/admin/materials.js';
import countryRoutes from './routes/admin/countries.js';
import carrierRoutes from './routes/admin/carriers.js';
import sizeRoutes from './routes/admin/sizes.js'; 
import categoryRoutes from './routes/admin/categories.js'; 
import subCategoryRoutes from './routes/admin/subCategories.js'; 
import innerCategoryRoutes from './routes/admin/innerCategories.js';
import innerCategory2Routes from './routes/admin/innerCategories2.js';


//DATABASE SEEDER IMPORT
import seederRoutes from './routes/seeder/seeder.js';


//AUTHENTICATION IMPORT
import authRoutes from './routes/auth/auth.js';

//AUTH MIDDLEWARE IMPORT
import { CheckAuth } from './middleware/checkAuth.js';

//USERS ROUTES IMPORT
import postRoutes from './routes/posts/posts.js';

//SHOP ROUTES IMPORT
import shopRoutes from './routes/shops/shops.js';

//PROFILE ROUTES IMPORT
import profileRoutes from './routes/profile/profile.js';

//FOLLWOER ROUTES IMPORT
import followersRoutes from './routes/followers/followers.js';

//PRODUCT ROUTES IMPORT
import productRoutes from './routes/products/products.js';

//FAVORITES ROUTE IMPORT
import favoriteRoutes from './routes/favorites/favorites.js';

//FEED ROUTES IMPORT
import feedsRoutes from './routes/feeds/feeds.js';
 
//PUBLIC PROFILE IMPORT
import publicProfileRoutes from './routes/publicProfile/publicProfile.js';

//COLLECTION ROUTE IMPORT
import collectionRoutes from './routes/collections/collections.js';

// CART ROUTE IMPORT
import cartRoutes from './routes/cart/cart.js';

//DELIVERY METHOD ROUTE IMPORT
import deliveryRoutes from './routes/delivery-method/index.js';

//LISTING ROUTES IMPORT
import listingRoutes from './routes/seller/listing/listing.js';

// ORDER ROUTES IMPORT
import ordersRoutes from './routes/seller/orders/orders.js';

//PRODUCT FILTER ROUTE IMPORT
import productFilterRoutes from './routes/product-filter/product-filter.js';

// UPLOAD IMAGE AND VIDEO

import uploadRoutes from './routes/upload/upload.js';

import CommentRoutes from './routes/comments/comments.js';


// ******************** SOCKET CONTROLLER IMPORT *******************

//COMMENT IMPORT
import postComments from './io_controller/comments.js';

//POST LIKE AND UNLIKE IMPORT
import ioPosts from './io_controller/post.js';



// ******************** END SOCKET CONTROLLER IMPORT *******************


//ALL ADMIN ROUTE
app.use('/admin/colors', colorRoutes);
app.use('/admin/brands', brandRoutes);
app.use('/admin/seasons', seasonRoutes);
app.use('/admin/materials', materialRoutes);
app.use('/admin/countries', countryRoutes);
app.use('/admin/carriers', carrierRoutes);
app.use('/admin/sizes', sizeRoutes);
app.use('/admin/categories', categoryRoutes);
app.use('/admin/sub-categories', subCategoryRoutes);
app.use('/admin/inner-categories', innerCategoryRoutes);
app.use('/admin/inner-categories2', innerCategory2Routes);

//DATABASE SEEDER ROUTE
app.use('/seeder', seederRoutes);

//AUTHENTICATION ROUTE
app.use('/user', authRoutes);

//USER POST ROUTE
app.use('/user/posts', postRoutes);

//USER SHOP ROUTE
app.use('/user/shops', shopRoutes);

//USER PROFILE ROUTE
app.use('/user/profile', profileRoutes);

//FOLLOWER ROUTE
app.use('/followers', followersRoutes);

//PRODUCT ROUTE
app.use('/products', productRoutes);

//FAVORITE ROUTE
app.use('/favorites', favoriteRoutes);

//FEED ROUTE
app.use('/feed', feedsRoutes);

//PUBLIC PROFILE ROUTE
app.use('/public-profile', publicProfileRoutes);

//COLLECTION ROUTE
app.use('/collections', collectionRoutes);

// CART ROUTE
app.use('/cart', cartRoutes);

// DELIVERY ROUTE
app.use('/delivery-method', deliveryRoutes);

// LISTING ROUTE
app.use('/seller/listing', listingRoutes);

// ORDERS ROUTE
app.use('/seller/orders', ordersRoutes);

//PRODUCT FILTER ROUTE
app.use('/product/filter', productFilterRoutes);

// UPLOAD IMAGE AND VIDEO
app.use('/upload', uploadRoutes);



//COMMENT ROUTE
app.use('/comments', CommentRoutes);

//MONGODB DATABASE CONNECTION
const PORT = process.env.PORT || 5000;
mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser:true, useUnifiedTopology:true })
    .then( () => {server.listen(PORT, () => console.log(`Server running on port: ${PORT}`));} )
    .catch( (error) => console.log(error.message));
mongoose.set('useFindAndModify', false);


// ************SOCKET SERVER START ***************

const server = createServer(app); 
const io = new Server(server);


let userList = []

io.on("connection", (socket) => {
	
	console.log("Socket connected");

    socket.emit("hello", "return form socket");

	if (userList.indexOf(socket.id) === -1) {
	  	userList.push(socket.id)
	}


	//ADD LIKE AND UNLIKE

	socket.on('action', async (action) => {

		switch (action.type) {
		  	case 'SOCKET_ACTION_ADD_LIKE_TO_POST':
				const addRemoveLike = await ioPosts.addRemovePostLike(action)
		
				if (addRemoveLike.success) {

					for (let i = 0; i < userList.length; i++) {
						socket.to(userList[i]).emit('action', addRemoveLike)
					}
					return socket.emit('action', addRemoveLike)
				}

			case 'SOCKET_ACTION_ADD_COMMENT_TO_POST':
				const saveComment = await postComments.createComment(action)
				if(saveComment.success){

					for( let i = 0; i< userList.length; i++){
						socket.to(userList[i]).emit("action",saveComment)
					}
					return socket.emit("action", saveComment);
				}

			case 'SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST':
				const replayComment = await postComments.replayComment(action)
				if(replayComment.success){

					for( let i = 0; i< userList.length; i++){
						socket.to(userList[i]).emit("action",replayComment)
					}
					return socket.emit("action", replayComment);
				}
				
				
			case 'SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST':
				const commentRemove = await postComments.removeComment(action)
				if(commentRemove.success){

					for( let i = 0; i< userList.length; i++){
						socket.to(userList[i]).emit("action",commentRemove)
					}
					return socket.emit("action", commentRemove);
				}
			case 'SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST':
				const commentEdit = await postComments.editComment(action)
				if(commentEdit.success){

					for( let i = 0; i< userList.length; i++){
						socket.to(userList[i]).emit("action",commentEdit)
					}
					return socket.emit("action", commentEdit);
				}		

		}
	})


  	//ADD REPLAY TO POST

  	socket.on("addReplay", async (data) => {

    	const saveReplay = await postComments.replayComment(data)

    	if(saveReplay.success){

    		socket.emit("getReplay", saveReplay.replay);
    		for( let i = 0; i< userList.length; i++){
    			socket.to(userList[i]).emit("getReplay",saveReplay.replay)
    		}
    	}
  	});


	
});


//socket disconnect
io.on('disconnect', function ( socket ) {

    const index = userList.indexOf(socket.id);

    if (index > -1) {
	  userList.splice(index, 1);
	}
});


//console.log(userList)

