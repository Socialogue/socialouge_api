import UserModel from '../models/users.js';
import CommentModel from '../models/comments.js';
import PostModel from '../models/posts.js';
import mongoose from 'mongoose';

const postComments = {

    createComment : async ( data ) => {

        const comment = data;
        let newComment = new CommentModel(comment);
        const postId = data.post;
        try {
        
            await newComment.save();

            const addCommentToPost = await PostModel.findOneAndUpdate(

                { _id: postId },

                { $push: { comments: newComment._id  }, $inc: { totalComment: 1, grandTotalComment: 1 } }
            );
            
            const totalComment = await PostModel.findOne(

                { _id: postId },
                { totalComment: 1, grandTotalComment: 1 }
            );

            let commentedUser = await UserModel.findOne(
                { _id: data.user },
                { fullName:1, slug:1, profileImage: 1 }

            )
            
            newComment = JSON.parse(JSON.stringify(newComment));
            newComment.postOwner = data.postOwner;
            newComment.user = commentedUser;
            newComment.totalComment = totalComment.totalComment
            newComment.grandTotalComment = totalComment.grandTotalComment
            return {
                type:'SOCKET_ACTION_ADD_COMMENT_TO_POST_RES',
                success:true,
                data: newComment, 
                message:'success'
            }
            

        } catch (error) {

            return {
                success:false,
                message: error.message 
            }
        }
        
    },

    removeComment: async ( data ) => {

        try {

            const postId = data.postId 
            const comment = data.comment
            const reply = data.reply
            const status = data.status // true means comment and false reply

            if( status ){

                const removeCommentFromParent = await PostModel.findOneAndUpdate(
                    { _id:postId },
                    { "$pull": { "comments": comment }, $inc:{ totalComment: -1 } }
                )
                
                const checkReply = await CommentModel.findOne(
                    { _id: comment },
                    { totalReply: 1 }
                )
                
                let calGrandTotal = 1;
                if( checkReply ){
                    calGrandTotal += checkReply.totalReply
                }
                const updateGrandTotal = await PostModel.findOneAndUpdate(
                    { _id:postId },
                    { $inc:{ grandTotalComment: -calGrandTotal } }
                )

                const totalComment = await PostModel.findOne(

                    { _id: postId },
                    { totalComment: 1, grandTotalComment: 1 }
                );
    
                data.grandTotalComment = totalComment.grandTotalComment
                data.totalComment = totalComment.totalComment
                const removeComment = await CommentModel.deleteOne(
                    { _id: comment }
                )
            } else {

               
                const removeReplay = await CommentModel.findOneAndUpdate(
                    { _id: comment },
                    { $pull: { replay: { _id: reply } }, $inc:{ totalReply: -1 } } 
                );

                const updateGrandTotal = await PostModel.findOneAndUpdate(
                    { _id:postId },
                    { $inc:{ grandTotalComment: -1 } }
                )

                const totalComment = await PostModel.findOne(

                    { _id: postId },
                    { totalComment: 1, grandTotalComment: 1 }
                );

                 const totalReply = await CommentModel.findOne(
    
                    { _id: comment},
                    { totalReply: 1 }
                );

                data.grandTotalComment = totalComment.grandTotalComment
                data.totalReply = totalReply.totalReply

            }

            return {
                type:'SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST_RES',
                success:true,
                data: data, 
                message:'success'
            }

        } catch (error) {

            return {
                success:false,
                message: error.message 
            }
        }
    },


    replayComment: async ( data ) => {

        let replay = {
            _id:mongoose.Types.ObjectId(),
            user: data.user,
            text: data.text,
            mention: data.mention,
            createdAt: new Date(),
        }

        try {

            const addGrandTotal = await PostModel.updateOne(

                { _id: data.postId },

                { $inc: { grandTotalComment: 1 } }
            );

            
            const addReplay = await CommentModel.findOneAndUpdate(

                { _id: data.commentId},

                { $push: { replay: replay  }, $inc:{ totalReply: 1 } }
            );

            
            

            const totalComment = await PostModel.findOne(

                { _id: data.postId },
                { totalComment: 1, grandTotalComment: 1 }
            );

            const totalReply = await CommentModel.findOne(

                { _id: data.commentId},
                { totalReply: 1 }
            );
            
            replay.grandTotalComment = totalComment.grandTotalComment
            replay.totalReply = totalReply.totalReply
            replay.postId = data.postId;
            replay.postOwner = data.postOwner;
            replay.commentOwner = data.commentOwner;
            replay.commenterName = data.commenterName;
            replay.commentId = data.commentId;

            const repliedUser = await UserModel.findOne(
                { _id: data.user },
                { fullName: 1, profileImage: 1, slug: 1}
            )

            replay.user = repliedUser

            if( data.mention ){
                const mantionUser = await UserModel.findOne(
                    { _id: data.mention },
                    { fullName: 1, profileImage: 1, slug: 1}
                )
                if(mantionUser){
                    replay.mention = mantionUser
                }
            }

            console.log(replay)
            return {
                type:'SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST_RES',
                success:true,
                data: replay, 
                message:'success'
            }

        } catch (error) {

            return {
                success:false,
                message: error.message 
            }
        }
    },

    editComment: async ( data ) => {

        try {

            const postId = data.postId 
            const comment = data.comment
            const reply = data.reply
            const text = data.text
            const status = data.status // true means comment and false reply

            if( status ){

                const editComment = await CommentModel.updateOne(
                    { _id: comment },
                    { $set: { "text":text } }
                )
            } else {

                const editReply = await CommentModel.updateOne(
                    { _id: comment, "replay._id": reply },
                    { $set: { "replay.$.text": text  } } 
                );

            }

            return {
                type:'SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST_RES',
                success:true,
                data: data, 
                message:'success'
            }

        } catch (error) {

            return {
                success:false,
                message: error.message 
            }
        }
    },




}
export default postComments; 