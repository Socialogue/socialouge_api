import UserModel from '../models/users.js';
import CommentModel from '../models/comments.js';
import PostModel from '../models/posts.js';
import mongoose from 'mongoose';


const ioPost = {

	addRemovePostLike : async ( req ) => {
		try {
			
	        const likeData = req;
	        let message = '';
			let flag = true;
	        let checkItem = await PostModel.findOne(
	            { _id: req.postId, "postLikes.userId": req.userId}
	        )
	        if(!checkItem){
	            const addLike = await PostModel.findOneAndUpdate(
	                {_id: req.postId},
	                { $push: { postLikes: likeData  }, $inc: { totalLike: 1 } }
	            );
	            const liked = await UserModel.findOneAndUpdate(
	                {_id: req.userId},
	                { $push: { likedPost: likeData  }, $inc: { totalLikedPost: 1} }
	            );
	            message = 'Like Added!';
	        }else{ 
				
	            const removeLike = await PostModel.findOneAndUpdate(
	                {_id: req.postId},
	                { $pull: { postLikes: { userId: req.userId}  }, $inc: { totalLike: -1 } }
	            );
	            const removeliked = await UserModel.findOneAndUpdate(
	                {_id: req.userId},
	                { $pull: { likedPost: { postId: req.postId}  }, $inc: { totalLikedPost: -1 } }
	            );
	            message = 'Remove Like!';
				flag = false;
	        }

	        let newTotal = await PostModel.findOne(
	            { _id: req.postId },
	            { totalLike: 1 }
	        )
			
			req.totalLike = newTotal.totalLike
			req.liked = flag;

	        return {
				type:'SOCKET_ACTION_ADD_LIKE_TO_POST_RES',
                success:true,
                data: req, 
                message:message
            }
	    } catch (error) {

	        return {
                success:false,
                message: error.message 
            }
	    }
	}
}

export default ioPost;