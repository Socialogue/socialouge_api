import crypto from 'crypto';
const iv = crypto.randomBytes(16);

const encodeDecode = {
    encode : async (text) => {
        const cipher = crypto.createCipheriv(process.env.CRYPTO_ALG, process.env.CRYPTO_SECRET_KEY, iv);
        const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
        const encryptedString = encrypted.toString('hex');
        return {
            iv: iv.toString('hex'),
            content: encrypted.toString('hex')
        };
        
    },
    decode: async (hash) => {
        const decipher = crypto.createDecipheriv(process.env.CRYPTO_ALG, process.env.CRYPTO_SECRET_KEY, Buffer.from(hash.iv, 'hex'));
        const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
        return decrpyted.toString();
    }
}
export default encodeDecode; 