import bcrypt from 'bcrypt';

const hashPassword = {
    getHashedPassword : async (password) => {
        const pass = bcrypt.hash(password, 12)
        return pass;
    }
}
export default hashPassword;