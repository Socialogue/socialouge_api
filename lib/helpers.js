import SlugModel from '../models/slug.js';

const helpers = {
    makeSlug : async ( text, model) => {
        text = text.toLowerCase();
        text = text.replace(/[^a-zA-Z0-9]+/g,'-');
        let checkSlug = true;
        let slug = '';
        let slugId = '';
        let loop = 0;
        while( checkSlug ) {
            if( loop == 0){
                slug = text;
            }else{
                slug = text+'-'+loop;
            }
            const checkItem = await SlugModel.findOne(
                { slug: slug }
            )

            if( checkItem ) {
                loop++;

            }else{

                let data = {
                    slug: slug,
                    model: model
                }
                const newSlug = new SlugModel(data);
                await newSlug.save();
                slugId = newSlug._id;
                checkSlug = false;
            }

        }

        return { slug, slugId };
    },

    updateSlug : async ( text, model, slId ) => {
        text = text.toLowerCase();
        text = text.replace(/[^a-zA-Z0-9]+/g,'-');
        let checkSlug = true;
        let slug = '';
        let slugId = '';
        let loop = 0;
        while( checkSlug ) {
            if( loop == 0){
                slug = text;
            }else{
                slug = text+'-'+loop;
            }
            const checkItem = await SlugModel.findOne(
                { slug: slug, _id:{ $ne: slId } }
            )

            if( checkItem ) {
                loop++;

            }else{

                if( slug != text){
                    const updateSlug = await SlugModel.updateOne(
                        { _id: slId },
                        { $set: { slug: slug } }
                    );
                }
                slugId = slId;
                checkSlug = false;
            }

        }

        return { slug, slugId };
    }
}
export default helpers; 