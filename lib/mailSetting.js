import nodemailer from 'nodemailer';
import { fileURLToPath } from 'url';
import path from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const mailSetting = {
    transporter : async () => {
        return nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.MAIL_HOST_USER,
                pass: process.env.MAIL_HOST_PASS
            }
        });
    },
    handlebarOptions: async () => {
        return {
            viewEngine: {
                extName: ".handlebars",
                partialsDir: path.join(__dirname, "../views/mail"),
                defaultLayout: false,
            },
            viewPath: path.join(__dirname, "../views/mail"),
            extName: ".handlebars",
        }
    }
}
export default mailSetting; 