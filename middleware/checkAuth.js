import jwt from 'jsonwebtoken';

export const CheckAuth = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        req.body.authUserId = decoded.userId;
        //console.log(req.body.authUserId);
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Authentication Failed!'
        })
    }
    
}