import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const brandSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Brand is required']
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

brandSchema.plugin(mongoosePaginate);
const BrandModel = mongoose.model('Brand', brandSchema);
export default BrandModel;