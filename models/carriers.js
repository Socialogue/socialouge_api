import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const CarrierSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Carrier is required']
    },
    countryId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Country is required'],
        ref:"Country"
    },
    createdAt: {
        type: Date,  
        default: new Date()
    }
});

CarrierSchema.plugin(mongoosePaginate);
const CarrierModel = mongoose.model('Carrier', CarrierSchema);
export default CarrierModel; 