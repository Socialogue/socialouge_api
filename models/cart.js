import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const cartSchema = mongoose.Schema({
    
    clientId: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'  
    },
    finalPrice: {
        type: String
    },
    softDelete: {
        type: Number,
        default: 0
    },
    products: [
        {
            shopId: {
                type: mongoose.Schema.Types.ObjectId,
                ref:'Shop'  
            },
            shopOwnerId: {
                type: mongoose.Schema.Types.ObjectId,
                ref:'User'  
            },
            productId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'Product'
            },
            price:{
                type: String
            },
            color:{},
            size:{
                type: String
            },
            quantity:{
                type: String
            },
            vat:{
                type: String
            },
            totalPrice:{
                type: String
            },
            vatAmmount:{
                type: String
            },
            grandTotal:{
                type: String
            },
            originalProduct:{}
        }
    ],
    createdAt: {
        type: Date,
        default: new Date()
    }
});

const CartModel = mongoose.model('Cart', cartSchema);
export default CartModel; 