import mongoose from 'mongoose';

const CategorySchema = mongoose.Schema({
    name: {
        type: String,
    },
    slug: {
        type: String
    },
    slugId: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    subCategory:[
        {
            name:{
                type: String
            },
            slug: {
                type: String
            },
            slugId: {
                type: mongoose.Schema.Types.ObjectId
            },
            color:{
                type: Boolean,
                default: false
            },
            sizes:[],
            createdAt: {
                type: Date,
                default: new Date()
            },
            innerCategory:[
                {
                    name:{
                        type: String
                    },
                    slug: {
                        type: String
                    },
                    slugId: {
                        type: mongoose.Schema.Types.ObjectId
                    },
                    color:{
                        type: Boolean,
                        default: false
                    },
                    sizes:[],
                    createdAt: {
                        type: Date,
                        default: new Date()
                    }
                }
            ]
        }
    ],
    color:{
        type: Boolean,
        default: false
    },
    sizes:[]
});

const CategoryModel = mongoose.model('Category', CategorySchema);
export default CategoryModel;