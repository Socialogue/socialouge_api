import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const colorSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Color is required']
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

colorSchema.plugin(mongoosePaginate);
const ColorModel = mongoose.model('Color', colorSchema);
export default ColorModel; 