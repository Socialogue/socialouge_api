import mongoose from 'mongoose';

const CommentSchema = mongoose.Schema({

    post: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Post'
    },

    user: {
    	type: mongoose.Schema.Types.ObjectId,
    	ref:'User'
    },

    text: {
    	type: String
    },

    replay:[
    	{
    		user:{
    			type: mongoose.Schema.Types.ObjectId,
    			ref:'User'
    		},
    		text:{
    			type: String
    		},
    		mention:{
    			type: mongoose.Schema.Types.ObjectId,
    			ref:'User'
    		},
    		createdAt: {
		        type: Date,
		        default: new Date()
		    }
    	}
    ],

	totalReply:{
		type: Number,
		default: 0
	},

    createdAt: {
        type: Date,
        default: new Date()
    }

});

const CommentModel = mongoose.model('Comment', CommentSchema);
export default CommentModel; 