import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const countrySchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Country is required']
    },
    code: {
        type: String
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

countrySchema.plugin(mongoosePaginate);
const CountryModel = mongoose.model('Country', countrySchema);
export default CountryModel; 