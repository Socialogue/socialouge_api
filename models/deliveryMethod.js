import mongoose from 'mongoose';

const DeliveryMethodSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Material is required']
    },
    fromDay:{
        type: Number
    },
    toDay:{
        type: Number
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

const DeliveryMethodModel = mongoose.model('DeliveryMethod', DeliveryMethodSchema);
export default DeliveryMethodModel; 