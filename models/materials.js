import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const MaterialSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Material is required']
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

MaterialSchema.plugin(mongoosePaginate);
const MaterialModel = mongoose.model('Material', MaterialSchema);
export default MaterialModel; 