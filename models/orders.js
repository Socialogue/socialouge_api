import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const OrderSchema = mongoose.Schema({

    shop: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Shop'
    },

    user: {
		type: mongoose.Schema.Types.ObjectId,
        ref:'User'
	},

    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    
    orderNumber: {
        type: String
    },

    productName: {
        type: String
    },

    totalCost: {
        type: String
    },

    orderStatus: {
        type: String,
        default: 'New'
    },

    model: {
        type: String
    },

    quantity: {
        type: String
    },

    orderDate: {
        type: Date,
        default: new Date()
    },
    
    shippedDate: {
        type: Date,
    },

    productImage:{
        type: String
    },

    paymentMethod:{
        type: String
    },

    variation:{
        type: String
    },

    selectedProduct:{},

    originalProduct:{}

});

OrderSchema.plugin(mongoosePaginate);

const OrderModel = mongoose.model('Order', OrderSchema);

export default OrderModel;