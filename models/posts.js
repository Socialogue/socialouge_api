import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
const PostSchema = mongoose.Schema({
    userId: {
		type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'User'
	},
    shopId: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Shop'
    },
    content: {
        type: String
    },
    images:[
        {
            url:{
                type: String,
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    videos:[
        {
            url:{
                type: String,
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    createdAt: {
        type: Date,
        default: new Date()
    },
    comments:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ],
    totalComment:{
        type: Number,
        default: 0
    },
    grandTotalComment:{
        type: Number,
        default: 0
    },

    postLikes:[
        {
            userId:{
                type: mongoose.Schema.Types.ObjectId
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    totalLike:{
        type: Number,
        default: 0
    },
    tags:[
        {
            productId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'Product'
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
});
PostSchema.plugin(mongoosePaginate);
const PostModel = mongoose.model('Post', PostSchema);
export default PostModel;