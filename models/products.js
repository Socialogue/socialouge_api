import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const ProductSchema = mongoose.Schema({
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },

    shopId:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Shop'
    },

    category:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Category'
    },

    subCategory:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Category'
    },

    innerCategory:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Category'
    }, 

    title:{
        type: String
    },

    slug:{
        type: String
    },

    slugId: {
        type: mongoose.Schema.Types.ObjectId
    },

    mainImage:{
        type: String
    },

    variationImages:[
        {
            colorId: {
                type: String,
            },

            imageUrl:{
                type: String
            },

            name:{
                type: String
            }
        }
    ], 
    additionalImage:[],

    shippingOptions:{

        country:{
            type: String
        },

        processingTime:{
            type: Number
        },

        templateName:{
            type: String,
        },

        deliveryTimeFrom:{
            type: Number
        },

        deliveryTimeTo:{
            type: Number
        },

        oneItemCost:{
            type: Number
        },

        additionalItemCost:{
            type: Number
        },

        templateId:{
            type: String //mongoose.Schema.Types.ObjectId
        },

        carrier:{
            type: String //mongoose.Schema.Types.ObjectId,
        },

        carrier:{
            type: String //mongoose.Schema.Types.ObjectId
        },

        carrierName:{
            type: String
        },

        freeShipping:{
            type: Boolean,
        },

        int_shipping:{
            type: String
        }
    },

    hasIntlShipping:{
        type: Boolean
    },

    intlShippingOptions:[
        {
            shipToCountries:[],

            oneItemCost:{
                type: Number
            },

            additionalItemCost:{
                type: Number
            },

            deliveryTimeFrom:{
                type: Number
            },

            deliveryTimeTo:{
                type: Number
            },

            shippingCarrierId:{
                type: String //mongoose.Schema.Types.ObjectId
            },

            carrierName:{
                type:String
            },

            templateName:{
                type:String
            },

            templateId:{
                type: String //mongoose.Schema.Types.ObjectId
            },

            freeShipping:{
                type: Boolean
            },
        }
    ],

    totalVariations:{
        type: Number
    },

    variations:[
        {
            sizes:{},

            color:{},

            colorImage:{
                type: String
            },

            sku:{
                type: String
            },

            quantity:{
                type: Number
            },

            price:{
                type: Number
            }
        }
    ],

    description:{
        type: String
    },

    materials:[],

    seasons:[],

    tags:[],

    followers:[
        {
            userId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'User'
            },
            followedAt: {
                type: Date,
                default: new Date()
            }
        }
    ],

    following:[
        {
            userId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'User'
            },
            followedAt: {
                type: Date,
                default: new Date()
            }
        }
    ],

    createdAt: {
        type: Date,
        default: new Date()
    }

});

ProductSchema.plugin(mongoosePaginate);
const ProductModel = mongoose.model('Product', ProductSchema);
export default ProductModel;