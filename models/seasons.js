import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const seasonSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Season is required']
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

seasonSchema.plugin(mongoosePaginate);
const SeasonModel = mongoose.model('Season', seasonSchema);
export default SeasonModel; 