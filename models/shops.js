import mongoose from 'mongoose';

const ShopSchema = mongoose.Schema({

	userId: {
		type: mongoose.Schema.Types.ObjectId,
        ref:'User'
	},

    shopName: {
        type: String,
    },

    slug: {
        type: String
    },

    slugId: {
        type: mongoose.Schema.Types.ObjectId
    },

    itemForSell: {
        type: String,
    },

    countryCode: {
        type: String,
    },

    phone: {
        type: String,
    },

    companyName: {
        type: String,
    },

    companyId: {
        type: String,
    },

    companyVat: {
        type: String,
    },

    companyEmail: {
        type: String,
    },

    registeredCompanyAddress: {
        type: String,
    },

    socialMedia1: {
        type: String,
    },

    socialMedia2: {
        type: String,
    },

    whereDidYouHere: {
        type: String
    },

    profileImage:{
        type: String
    },

    coverImage: {
        type: String
    },

    followers:[
        {
            userId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'User'
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    totalFollowers:{
        type: Number,
        default: 0
    },

    shipFromTemplate:[
        {

            country:{
                type: String
            },

            processingTime:{
                type: String
            },

            carrier:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Carrier'
            },

            carrierName:{
                type: String
            },

            templateName:{
                type: String,
            },

            deliveryTimeFrom:{
                type: Number
            },

            deliveryTimeTo:{
                type: Number
            },

            oneItemPrice:{
                type: Number
            },

            additionalItemPrice:{
                type: Number
            },

            freeShipping:{
                type: Boolean,
            }
        }
    ],

    shipToTemplate:[
        {
            country:{
                type: String
            },

            processingTime:{
                type: String
            },

            carrier:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Carrier'
            },

            carrierName:{
                type: String
            },

            templateName:{
                type: String,
            },

            deliveryTimeFrom:{
                type: Number
            },

            deliveryTimeTo:{
                type: Number
            },
            
            oneItemPrice:{
                type: Number
            },

            additionalItemPrice:{
                type: Number
            },

            freeShipping:{
                type: Boolean,
            }
        }
    ],

    createdAt: {
        type: Date,
        default: new Date()
    }
});

const ShopModel = mongoose.model('Shop', ShopSchema);
export default ShopModel;