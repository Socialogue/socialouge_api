import mongoose from 'mongoose';
// Category
// Men Clothes Size - 1
// Men Pents Size - 2
// Men Shoes Size - 3
// Women Clothes Size - 4
// Women Pents Size - 5
// Women Shoes Size - 6
// Kid Boys Clothes Size - 7
// Kid Boys Pents Size - 8
// Kid Boys Shoes Size - 9
// Kid Girls Clothes Size - 10
// Kid Girls Pents Size - 11
// Kid Girls Shoes Size - 12
// Babies Clothes Size -13
// Babies Shoes Size - 14
const SizeSchema = mongoose.Schema({
    eu: {
        type: String
    },
    lt: {
        type: String
    },
    uk: {
        type: String
    },
    us: {
        type: String
    },
    category: {
        type: Number,
        required: [true, 'Size Category is required']
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

const Sizes = mongoose.model('size', SizeSchema);
export default Sizes;
 