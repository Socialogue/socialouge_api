import mongoose from 'mongoose';

const SlugSchema = mongoose.Schema({
    slug: {
        type: String
    },
    model: {
       type: String
    },
    createdAt: {
        type: Date,  
        default: new Date()
    }
});

const SlugModel = mongoose.model('slug', SlugSchema);
export default SlugModel; 