import mongoose from 'mongoose';
const UserSchema = mongoose.Schema({
    fullName: {
        type: String,
        required:true
    },
    slug:{
        type: String
    },
    slugId: {
        type: mongoose.Schema.Types.ObjectId
    },
    nickName: {
        type: String,
    },
    username: {
        type: String,
    },
    gender: {
        type: String,
    },
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    country:{
        type: String
    },
    phone: {
        type: String,
    },
    profileImage: {
        type: String,
    },
    coverImage: {
        type: String,
    },
    role: {
        type: Number,
        default: 1  // 1 for user and 2 for admin
    },
    accountType: {
        type: Number,  //Email - 1, Phone - 2, facebook - 3, google - 4
        required: true,
    },
    verified: {
        type: Boolean,
        default: false
    },
    delivery:[
        {
            name: {
                type: String
            },
            surname: {
                type: String
            },
            country: {
                type: String
            },
            address: {
                type: String
            },
            zipCode: {
                type: String
            },
            phone: {
                type: String
            },
            email: {
                type: String
            },
            billing: {
                type:Number
            }
        }
    ],
    likedPost:[
        {
            postId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'Post'
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    totalLikedPost:{
        type: Number,
        default: 0
    },
    followers:[
        {
            userId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'User'
            },
            shopId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'Shop'
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    
    following:[
        {
            userId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'User'
            },
            shopId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'Shop'
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    totalFollowers:{
        type: Number,
        default: 0
    },
    totalFollowing:{
        type: Number,
        default: 0
    },
    
    favoriteProduct:[
        {
            productId:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'Product'
            },
            createdAt: {
                type: Date,
                default: new Date()
            }
        }
    ],
    TotalfavoriteProduct:{
        type: Number,
        default: 0
    },

    collections:[ 
        {
            name:{
                type: String
            },
            createdAt: {
                type: Date,
                default: new Date()
            },
            products:[],
            totalProduct:{
                type: Number
            }
        }
    ],
    
    lastPasswordChange:{
        type: Date
    },
    softDelete:{
        status:{
            type: Number,
            default: 0
        },
        date:{
            type: String
        }
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
    
});
const UserModel = mongoose.model('User', UserSchema);
export default UserModel; 