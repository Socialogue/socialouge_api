import mongoose from 'mongoose';

const VerificationSchema = mongoose.Schema({
    username: {
        type: String,
    },
    otp: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

const VerificationModel = mongoose.model('Verification', VerificationSchema);
export default VerificationModel; 