import express from 'express';
import { getBrands, createBrand, getBrandById, updateBrand, deleteBrand } from '../../controllers/admin/brands.js';
const router = express.Router();

//admin color routes
router.get('/', getBrands);
router.post('/create', createBrand);
router.get('/:brandId', getBrandById);
router.patch('/:brandId', updateBrand);
router.delete('/:brandId', deleteBrand);


export default router;