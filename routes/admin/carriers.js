import express from 'express';
import { getCarriers, createCarrier, getCarrierById, updateCarrier, deleteCarrier } from '../../controllers/admin/carriers.js';
const router = express.Router();

//admin color routes
router.get('/', getCarriers);
router.post('/create', createCarrier);
router.get('/:carrierId', getCarrierById);
router.patch('/:carrierId', updateCarrier);
router.delete('/:carrierId', deleteCarrier);


export default router;