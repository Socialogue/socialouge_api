import express from 'express';
import { getCategories, createCategory, getCategoryById, updateCategory, deleteCategory, catWithSubByCatId } from '../../controllers/admin/categories.js';
const router = express.Router();

//admin color routes
router.get('/', getCategories);
router.post('/create', createCategory);
router.get('/:categoryId', getCategoryById);
router.patch('/:categoryId', updateCategory);
router.delete('/:categoryId', deleteCategory);

router.get('/sub-categories/:categoryId', catWithSubByCatId);


export default router;