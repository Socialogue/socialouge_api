import express from 'express';
import { getColors, createColor, getColorById, updateColor, deleteColor } from '../../controllers/admin/colors.js';
const router = express.Router();

//admin color routes
router.get('/', getColors);
router.post('/create', createColor);
router.get('/:colorId', getColorById);
router.patch('/:colorId', updateColor);
router.delete('/:colorId', deleteColor);


export default router;