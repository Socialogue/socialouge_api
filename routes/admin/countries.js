import express from 'express';
import { getCountries, createCountry, getCountryById, updateCountry, deleteCountry } from '../../controllers/admin/countries.js';
const router = express.Router();

//admin color routes
router.get('/', getCountries);
router.post('/create', createCountry);
router.get('/:countryId', getCountryById);
router.patch('/:countryId', updateCountry);
router.delete('/:countryId', deleteCountry);


export default router;