import express from 'express';
import { createInnerCategory, innerCat2ByInnerCatId, getInnerCategoryById, innerCategoryDelete, updateInnnerCategory } from '../../controllers/admin/innerCategories.js';
const router = express.Router();

//admin color routes
// router.get('/', getCategories);
router.post('/create', createInnerCategory);
router.get('/inner-categories2/:innerCategoryId', innerCat2ByInnerCatId);
router.get('/:innerCategoryId', getInnerCategoryById);
router.patch('/:innerCategoryId', updateInnnerCategory);
router.delete('/:innerCategoryId', innerCategoryDelete);


export default router;