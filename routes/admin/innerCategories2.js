import express from 'express';
import { createInnerCategory2, getInnerCategory2ById,innerCategory2Delete, updateInnerCategory2 } from '../../controllers/admin/innerCategories2.js';
const router = express.Router();


//admin color routes
// router.get('/', getCategories);
router.post('/create', createInnerCategory2);
router.get('/:innerCategory2Id', getInnerCategory2ById);
router.patch('/:innerCategory2Id', updateInnerCategory2);
router.delete('/:innerCategory2Id', innerCategory2Delete);


export default router;