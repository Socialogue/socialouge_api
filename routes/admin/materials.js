import express from 'express';
import { getMaterials, createMaterial, getMaterialById, updateMaterial, deleteMaterial } from '../../controllers/admin/materials.js';
const router = express.Router();


//admin color routes
router.get('/', getMaterials);
router.post('/create', createMaterial);
router.get('/:materialId', getMaterialById);
router.patch('/:materialId', updateMaterial);
router.delete('/:materialId', deleteMaterial);



export default router;