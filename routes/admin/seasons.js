import express from 'express';
import { getSeasons, createSeason, getSeasonById, updateSeason, deleteSeason } from '../../controllers/admin/seasons.js';
const router = express.Router();

//admin color routes
router.get('/', getSeasons);
router.post('/create', createSeason);
router.get('/:seasonId', getSeasonById);
router.patch('/:seasonId', updateSeason);
router.delete('/:seasonId', deleteSeason);


export default router;