import express from 'express';
import { getSizes, getSizeByCategory, createSize, getSizeById, updateSize, deleteSize } from '../../controllers/admin/sizes.js';
const router = express.Router();

//admin color routes
router.get('/', getSizes);
router.get('/category/:categoryId', getSizeByCategory);
router.post('/create', createSize);
router.get('/:sizeId', getSizeById);
router.patch('/:sizeId', updateSize);
router.delete('/:sizeId', deleteSize);


export default router;