import express from 'express';
import { createSubCategory, deleteSubCategory, getSubCategoryById, innerCatBySubCatId, updateSubCategory } from '../../controllers/admin/subCategories.js';
const router = express.Router();

//admin color routes
// router.get('/', getCategories);
router.post('/create', createSubCategory);
router.get('/:subCategoryId', getSubCategoryById);
router.patch('/:subCategoryId', updateSubCategory);
router.delete('/:subCategoryId', deleteSubCategory);
router.get('/inner-category/:subCategoryId', innerCatBySubCatId);


export default router;