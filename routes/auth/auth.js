import express from 'express';
import { register, resendCode } from '../../controllers/auth/register.js';
import { login } from '../../controllers/auth/login.js';
import { verify } from '../../controllers/auth/verify.js';
import { forgetPassword, matchForgotPasswordOtp } from '../../controllers/auth/forgetPassword.js';
import { resetPassword } from '../../controllers/auth/resetPassword.js';
import { getUsers } from '../../controllers/auth/users.js';
// import { validate, validateRegister } from '../../validations/validations.js';
const router = express.Router();

router.get('/', getUsers);
router.post('/signup', register);
router.post('/resend-code', resendCode);
router.post('/verify', verify);
router.post('/login', login);
router.post('/forget-password', forgetPassword);
router.post('/forget-password/otp', matchForgotPasswordOtp);

router.post('/reset-password', resetPassword);

export default router;