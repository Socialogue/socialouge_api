import express from 'express';
import { addToCart,  udateCart, deleteFromCart, getCart} from '../../controllers/cart/cart.js';
import { CheckAuth } from '../../middleware/checkAuth.js';
const router = express.Router();

router.get('/get', CheckAuth, getCart);
router.post('/add', CheckAuth, addToCart);
router.patch('/update', CheckAuth, udateCart);
router.delete('/delete', CheckAuth, deleteFromCart);

export default router;