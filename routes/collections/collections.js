import express from 'express';
import { createCollection, addProduct, deleteProduct, specificProduct, getCollection, getAllCollection } from '../../controllers/collections/collections.js';
import { CheckAuth } from '../../middleware/checkAuth.js';
const router = express.Router();

router.post('/add', CheckAuth, createCollection);
router.post('/add-product', CheckAuth, addProduct);
router.delete('/delete-product', CheckAuth, deleteProduct);
router.get('/specific-product', CheckAuth, specificProduct);
router.get('/get-collections', CheckAuth, getCollection);
router.get('/all-collections', CheckAuth, getAllCollection);
export default router;