import express from 'express';
import { 
	getComment,
	getReply

} from '../../controllers/comments/comments.js';
const router = express.Router();

router.get('/get', getComment);
router.get('/reply', getReply);

export default router; 