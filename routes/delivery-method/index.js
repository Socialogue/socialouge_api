import express from 'express';
import { getDeliveryMethod, addDeliveryMethod, updateDeliveryMethod, deleteDeliveryMethod } from '../../controllers/delivery-method/index.js';
const router = express.Router();

router.get('/get', getDeliveryMethod);
router.post('/add', addDeliveryMethod);
router.patch('/update/:id', updateDeliveryMethod);
router.delete('/delete/:id', deleteDeliveryMethod);

export default router; 