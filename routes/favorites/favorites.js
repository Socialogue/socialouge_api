import express from 'express';
import { createFavorite, removeFavorite, getFavoriteProduct } from '../../controllers/favorites/favorites.js';
const router = express.Router();

router.post('/add', createFavorite);
router.delete('/delete', removeFavorite);
router.get('/get', getFavoriteProduct);
 
export default router;