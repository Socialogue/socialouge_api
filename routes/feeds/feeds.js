import express from 'express';
import { getProductAndPosts } from '../../controllers/feeds/feeds.js';
const router = express.Router();

router.get('/', getProductAndPosts);


export default router; 