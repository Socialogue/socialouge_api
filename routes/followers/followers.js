import express from 'express';
import { createFollowers, removeFollowers, getFollowingList, getFollowerList, createShopFollower, removeShopFollower } from '../../controllers/followers/followers.js';
const router = express.Router();


router.post('/add', createFollowers);
router.delete('/remove', removeFollowers);
router.get('/following-list', getFollowingList);
router.get('/follower-list', getFollowerList);

router.post('/shop/add-follower', createShopFollower);
router.delete('/shop/delete-follower', removeShopFollower);


export default router;  