import express from 'express';
import { getPosts, createPost, getPostById, updatePost, deletePost, getPostByUserId, createPostComment, createPostLike, getLikedPost, getPostByShopId } from '../../controllers/posts/posts.js';
const router = express.Router();
import { CheckAuth } from '../../middleware/checkAuth.js';

//admin color routes
router.get('/', getPosts);
router.post('/create', createPost);
router.post('/comment', createPostComment);
router.post('/like', createPostLike);
router.get('/like/get', getLikedPost);
router.get('/:postId', getPostById);
router.get('/user/:userId', getPostByUserId);
router.patch('/:postId', updatePost);
router.delete('/:postId', deletePost);
router.get('/get/by-shop/:shopId', getPostByShopId);

export default router; 