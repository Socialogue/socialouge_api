import express from 'express';
import { 
	createProduct, 
	getProductByUserId, 
	updateProduct, 
	deleteProduct, 
	getProductById, 
	getAllItems, 
	getProductByShopId,
	searchProduct 
} from '../../controllers/products/products.js';

const router = express.Router();

 
router.post('/add', createProduct);
router.post('/search', searchProduct);
router.get('/get', getProductByUserId);
router.get('/get/all-items/:shopId', getAllItems);
router.patch('/update/:productId', updateProduct);
router.delete('/delete/:productId', deleteProduct);
router.get('/details/:productId', getProductById);
router.get('/product-by-shop/:shopId', getProductByShopId);

export default router;