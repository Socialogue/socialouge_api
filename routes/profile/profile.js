import express from 'express';
import { CheckAuth } from '../../middleware/checkAuth.js';
import {
    getProfile, 
    updateProfile, 
    getProfileByArray, 
    sendOtpForChangeEmail,
    changeEmail,
    changePassword, 
    deleteAccount, 
    updateDeliveryInfo, 
    getCurrentProfileInfo,
    addDeliveryInfo,
    getDeliveryInfo,
    deleteDeliveryInfo,
    imageUpload
} from '../../controllers/profile/profile.js';
const router = express.Router();

// all Routes
router.get('/', getProfile);
router.patch('/update', CheckAuth, updateProfile);
router.get('/list', getProfileByArray);
router.post('/change-email/otp', CheckAuth, sendOtpForChangeEmail);
router.post('/change-email', CheckAuth, changeEmail);
router.post('/change-password', CheckAuth, changePassword);
router.post('/delete-account', CheckAuth, deleteAccount);

router.get('/get', CheckAuth, getCurrentProfileInfo);

router.get('/get-delivery-info', CheckAuth, getDeliveryInfo);
router.post('/add-delivery-info', CheckAuth, addDeliveryInfo);
router.patch('/update-delivery-info', CheckAuth, updateDeliveryInfo);
router.delete('/delete-delivery-info/:id', CheckAuth, deleteDeliveryInfo);

router.post('/image-upload', imageUpload);

export default router;  