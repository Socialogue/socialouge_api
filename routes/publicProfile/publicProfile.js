import express from 'express';
import {getUser, getPosts, getFavProduct,getFollowing } from '../../controllers/publicProfile/publicProfile.js';
const router = express.Router();


router.get('/:userId', getUser);
router.get('/post/:userId', getPosts);
router.get('/favorite-product/:userId', getFavProduct);
router.get('/following/:userId', getFollowing);


export default router; 