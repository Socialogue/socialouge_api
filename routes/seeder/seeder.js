import express from 'express';
import { createShop, createCategory, createCarrier, createPost,createProduct,createUser  } from '../../controllers/seeder/seeder.js';
const router = express.Router();



router.post('/user', createUser);
router.post('/shop', createShop);
router.post('/category', createCategory);
router.post('/carrier', createCarrier);
router.post('/post', createPost);
router.post('/product', createProduct);

export default router;