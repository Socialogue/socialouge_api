import express from 'express';
import {
	getListing,
	deleteListing,
	changeQuantityOrPrice
} from '../../../controllers/seller/listing/listing.js';
const router = express.Router();



router.get('/get/:shopId', getListing);
router.post('/change-quantity-or-price/', changeQuantityOrPrice);
router.delete('/delete/', deleteListing);


export default router;