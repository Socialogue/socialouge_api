import express from 'express';
import {
	getOrders,
	createOrder,
	changeOrderStatus
} from '../../../controllers/seller/orders/orders.js';
const router = express.Router();



router.get('/get/:shopId', getOrders);
router.post('/create-order/', createOrder);
router.post('/change-order-status/', changeOrderStatus);



export default router;