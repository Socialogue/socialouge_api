import express from 'express';
import { CheckAuth } from '../../middleware/checkAuth.js';
import { createShop, getShopByUserId, updateShop, getShopById, deleteShop, getProductByShop, addTempleteToShop, deleteTempleteFromShop, getAllShop } from '../../controllers/shops/shops.js';
const router = express.Router();

// router.get('/', getPosts);
router.post('/create', createShop);
router.get('/user/get-shops', CheckAuth, getShopByUserId);
router.get('/:shopId', getShopById);
router.patch('/:shopId', updateShop);
router.delete('/:shopId', deleteShop);
router.get('/products/get', getProductByShop);
router.get('/public/shops', getAllShop);

router.post('/add-templete', CheckAuth, addTempleteToShop);

router.delete('/templete/delete/:tempId/:status', deleteTempleteFromShop);


export default router;  