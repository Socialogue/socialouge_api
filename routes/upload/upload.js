import express from 'express';

import { uploadImage } from '../../controllers/upload/upload.js';
const router = express.Router();

// router.get('/', getPosts);
router.post('/image', uploadImage);



export default router;  