import {check, body, validationResult} from 'express-validator';
import fs from 'fs';

//checking for validation errors 
export const validate = (req, res, next) => {

    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }

    if(req.file !== undefined){
        fs.unlinkSync(req.file.path)
    }
    console.log("validation: ", req.body);
    return res.status(401).json({errors: errors.array()});
}


//validating registration data...
export const validateRegister = (req, res) => {
    return [
        check('name').isLength({min: 2, max: 20}).trim().escape().withMessage('Name is Required'),
    ];
}

